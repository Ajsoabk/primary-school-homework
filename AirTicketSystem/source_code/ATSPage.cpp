#include <iostream>
#include <windows.h>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <string>
#include <vector>
#include <conio.h>
#include <limits>
#include "ATS.h"
using namespace std;
string System::User_ID;
string System::F_num;
int System::Seat;
/*
光标往左移动一位
用于密码输入函数
*/
void Move_To_last()
{
	HANDLE hConsole=GetStdHandle(STD_OUTPUT_HANDLE);//获得标准输出窗口的句柄
	CONSOLE_SCREEN_BUFFER_INFO csbi;//准备接收输出缓冲区的信息

	if (GetConsoleScreenBufferInfo(hConsole, &csbi)&&csbi.dwCursorPosition.X-1>=0)
	{
        HANDLE hOut; 
        hOut=GetStdHandle(STD_OUTPUT_HANDLE);  //实例化句柄 hOut
        COORD pos={0,0};
        pos.X=csbi.dwCursorPosition.X-1;
        pos.Y=csbi.dwCursorPosition.Y;
        SetConsoleCursorPosition(hOut,pos);
	}
}
/*
返回用户输入的密码
可以接受退格
*/
string InputingPwd(){
    Hide_Cursor(false);
    char a[30]={'\0'}; 
    char c;
	int i=0;
	while (1){ 
		c = _getch(); //阻塞函数，获得用户输入的字符
		if (c == '\r') //当用户按下的是回车键，如果是则标记字符结尾并跳出循环
        {
            a[i]='\0';
            break; 
        }
		if(Clicked( 0x08 ))//当用户按下退格键
        {
            if(i)//光标不在最左边
            {
                Move_To_last();
                cout<<" ";
                Move_To_last();
                i--;
                a[i]='\0';
            }
        }
        else
        {
			a[i] = c;
			i++; 
			cout << "*";
		}
	}
    Hide_Cursor();
    return string(a);
}
/*
数学问题验证码生成函数
获取一个随机产生的pair对象，第一个元素是随机产生的数学问题，第二个元素是系统计算的结果
传入一个难度等级的参数，1~3从低到高逐渐变难
*/
pair<string, string> back_Vcode(int level)  // 1<=level<=3
{
    pair<string, string> kk;
    if(level==1)
    {   
        int ans=0;
        srand(time(NULL)); /*根据当前时间设置“随机数种子”*/
        int num1=rand()%100+1;
        int num2=rand()%100+1;
        ans=num1+num2;
        
        ostringstream questionout;
        ostringstream ansout;
        questionout<<num1<<" + "<<num2<<" = ";
        ansout<<ans;
        kk=make_pair(questionout.str(),ansout.str());
    }
    if(level==2)
    {
        int ans=0;
        srand(time(NULL)); /*根据当前时间设置“随机数种子”*/
        int num1=rand()%100+1;
        int num2=rand()%100+1;
        ans=num1*num2;
        
        ostringstream questionout;
        ostringstream ansout;
        questionout<<num1<<" x "<<num2<<" = ";
        ansout<<ans;
        kk=make_pair(questionout.str(),ansout.str());
    }
    if(level==3)
    {
        int ans=0;
        srand(time(NULL)); /*根据当前时间设置“随机数种子”*/
        int num1=rand()%100+1;
        int num2=rand()%100+1;
        int num3=rand()%100+1;
        ans=num1*num2+num3;
        
        ostringstream questionout;
        ostringstream ansout;
        questionout<<num1<<" x "<<num2<<" + "<<num3<<" = ";
        ansout<<ans;
        kk=make_pair(questionout.str(),ansout.str());
    }
    return kk;    
}
/*
邮箱发送函数
传入一个string类型的邮箱参数
返回自动生成的随机四位验证码
*/
string send_email(string mail){
    ofstream fout("send_mail.vbs");
    int a;
    string str;
    stringstream strout;
    srand((unsigned)time(NULL));
    a=rand()%10000;
    if(a<1000) a+=1000;

    fout<<"Dim sh "<<endl<<"Set sh = CreateObject(\"Wscript.Shell\") "<<endl<<"sh.Run \"cmd.exe\",1,False "<<endl;
    fout<<"WScript.Sleep 1000 "<<endl<<"sh.SendKeys (\"telnet smtp.sohu.com 25{ENTER}\")"<<endl;
    fout<<"WScript.Sleep 1000 "<<endl<<"sh.SendKeys (\"helo sohu.com{ENTER}\")"<<endl;
    fout<<"WScript.Sleep 1000 "<<endl<<"sh.SendKeys (\"auth login{ENTER}\")"<<endl;
    fout<<"WScript.Sleep 1000 "<<endl<<"sh.SendKeys (\"eXpianFlQHNvaHUuY29t{ENTER}\")"<<endl;
    fout<<"WScript.Sleep 1000 "<<endl<<"sh.SendKeys (\"VVJPTFRXWjk1SURFQQ=={ENTER}\")"<<endl;
    fout<<"WScript.Sleep 1000 "<<endl<<"sh.SendKeys (\"mail from:<yzbjqe@sohu.com>{ENTER}\")"<<endl;
    fout<<"WScript.Sleep 1000 "<<endl<<"sh.SendKeys (\"rcpt to:<"<<mail<<">{ENTER}\")"<<endl;
    fout<<"WScript.Sleep 1000 "<<endl<<"sh.SendKeys (\"data{ENTER}\")"<<endl;
    fout<<"WScript.Sleep 1000 "<<endl<<"sh.SendKeys (\"subject:identifying_code{ENTER}\")"<<endl;
    fout<<"WScript.Sleep 1000 "<<endl<<"sh.SendKeys (\"from:San_Bing_Ticket@sohu.com{ENTER}\")"<<endl;
    fout<<"WScript.Sleep 2000 "<<endl<<"sh.SendKeys (\"to:"<<mail<<"{ENTER}\")"<<endl;
    fout<<"WScript.Sleep 1000 "<<endl<<"sh.SendKeys (\"Your_identifying_code_is_"<<a<<"{ENTER}Please_use_it_in_5_minitues{ENTER}Have_a_good_day{ENTER}.{ENTER}\")"<<endl;
    fout<<"WScript.Sleep 1000 "<<endl<<"sh.SendKeys (\"quit{ENTER}\")"<<endl;
    fout<<"WScript.Sleep 1000 "<<endl<<"sh.SendKeys (\"exit{ENTER}\")"<<endl;
    fout.close();
    system("send_mail.vbs");
    system("del send_mail.vbs");
    strout<<a;
    str=strout.str();
    
    return str;
}
/*
属于文本框类的文本读入函数
*/
void Text_Box::inputing(){
    this->Print();
    Hide_Cursor(false);
    cin>>inputtext;
    Hide_Cursor();
}
/*
订单生成函数
获取需要输出的表单
*/
int Order::Display(Form &output){
    Button but1(&output,string("购票人:")+ name+string(" 所购航班号:")+ num);
    if(seat==0){
        Button but4(&output,string("经济舱"),1);
    }
    else if(seat==1){
        Button but4(&output,string("商务舱"),1);
    }
    else if(seat==2){
        Button but4(&output,string("头等舱"),1);
    }
    Button but4(&output,to_string(pri)+string("￥"),2);
    Button but2(&output,string("从")+ cit[0]+string("飞往")+ cit[1],3);
    Button but3(&output,string("  ")+ stime+string("出发")+etime+string("到达"),4);
    return 5;
}
int Flight::Display(Form &output,string command){
    int incre=0;
    Button but0(&output,string("航班号：")+num,incre);
    incre++;
    if(command[0]=='1'){
        Button but1(&output,string("航空公司：")+com,incre);
        incre++;
    }
    if(command[4]=='1'){
        Button but2(&output,string("机型：")+mod+string("飞行员姓名")+pil[0]+string(" ")+pil[1],incre);
        incre++;
    }
    if(command[3]=='1'){
        Button but3(&output,string("准点率:")+to_string(rat)+"%",incre);
        incre++;
    }
    if(command[1]=='1'){
        Button but4(&output,cit[0]+string("------->")+cit[1],incre);
        incre++;
        Button but5(&output,stime+string("        ")+etime,incre);
        incre++;
    }
    if(command[2]=='1'){
        Button but6(&output,string("经济舱剩余")+to_string(rem[0])+string("个座位,价格")+to_string(pri[0])+string("￥"),incre);
        incre++;
        Button but7(&output,string("商务舱剩余")+to_string(rem[1])+string("个座位,价格")+to_string(pri[1])+string("￥"),incre);
        incre++;
        Button but8(&output,string("头等舱剩余")+to_string(rem[2])+string("个座位,价格")+to_string(pri[2])+string("￥"),incre);
        incre++;
    }
    return incre+1;
}
string Sex_In_Chinese(bool sex){
    if(sex)return "男";
    else return "女";
}

string Status_To_Chinese(int sta){
    if(sta==0)return "普通用户";
    else if(sta==1)return "管理员";
    else return "VIP用户";
}

string Print_Underline(int n){
    ostringstream strout;
    for(int i=0;i<n;++i){
        strout<<'_';
    }
    for(int i=0;i<n;++i){
        strout<<'\b';
    }
    return strout.str();
}

int System::Welcome_Page(const Page& curpage)
{
    system("cls");
    Hide_Cursor();
    /*
    主块
    包含所有相对于主块定位的文本及页面跳转按钮
    */
    Form mainform(5,90);
    Button titlebut(&mainform,string("航空管理系统"));
    Button welcomebut(&mainform,string("欢迎来到航空管理系统！"),10,-5);
    Button loginbut(&mainform,string("已有账号？点此登录"),14,-3);
    Button signupbut(&mainform,string("没有账号？点此注册"),16,-3);
    mainform.Print();


    int command=0,flag;
    pair<int,int> coord;
    do
    {
        flag=Get_Signal(coord);
        if(flag==0)
        {
            if(Inside_Of(loginbut,coord))
            {
                Reverse_Color(loginbut);
                command = curpage.translate("login");
            }
            else if(Inside_Of(signupbut,coord))
            {
                Reverse_Color(signupbut);
                command = curpage.translate("signup");
            }
        }
    }while(command==0);
    return command;
};
int System::Log_In_Page(const Page& curpage)
{
    Hide_Cursor();
    system("cls");
    /*
    主块
    */
    Form mainform(5,10);
    Button freshbut(&mainform,string("@刷新页面"));
    Button backbut(&mainform,string("<返回上一级"),2);
    Button exitbut(&mainform,string("<退出系统 |"),19);
    Button titlebut(&mainform,string("登录界面"),0,83);
    mainform.Print();
    /*
    登录表单
    包含所需文本框以及提交按钮
    以及相对于登录表单定位的错误信息文本
    */
    Form loginform(15,83);
    Text_Box idinput(&loginform, string("用户ID：")+Print_Underline(15));
    Text_Box pwdinput(&loginform,string("  密码：")+Print_Underline(15),2);
    Button forgetbut(&loginform,string("忘记密码？点此找回密码"),4,3);
    Button errbut(&loginform,string(""),8,2);
    Button_Box loginbut(&loginform,string("| 登录 |"),6,10);
    loginform.Print();

    int command=0,flag=0,cnt=0,errcnt=0;
    pair<int,int> coord;
    do{
        flag=Get_Signal(coord);
        if(flag==2){
        }
        else if(flag==0){
            //对于在文本框的点击操作，直接读取到该文本框内
            if(Inside_Of(idinput,coord)){
                idinput.inputing();
            }
            else if(Inside_Of(pwdinput,coord)){
                pwdinput.Print();
                pwdinput.inputtext=InputingPwd();
            }
            //对于页面跳转按钮的点击操作，按钮闪烁后直接返回页面跳转命令
            else if(Inside_Of(freshbut,coord)){
                Reverse_Color(freshbut);
                command = curpage.translate("fresh");
            }
            else if(Inside_Of(backbut,coord)){
                Reverse_Color(backbut);
                command = curpage.translate("back");
            }
            else if(Inside_Of(exitbut,coord)){
                Reverse_Color(exitbut);
                command = curpage.translate("exit");
            }
            else if(Inside_Of(forgetbut,coord)){
                Reverse_Color(forgetbut);
                command = curpage.translate("forget");
            }
            /*
            对于提交按钮的点击操作：
            先按钮闪烁，再检查各种错误，
            将提示信息（错误或正确）打印，在登陆成功的执行语句后设置用户ID的全局变量
            然后直接返回跳转主页命令
            */
            else if(Inside_Of(loginbut,coord)){
                Reverse_Color(loginbut);
                if(Unique_ID(idinput.inputtext)){
                    errbut.text=    " 用户ID不存在,请重新输入   ";
                    errbut.Print();
                    cnt++;
                }
                else{
                    string pwd=Check_By_ID(idinput.inputtext);
                    if(pwd!=pwdinput.inputtext){
                        errbut.text="  密码错误，请重新输入   ";
                        errbut.Print();
                        errcnt++;
                    }
                    else{
                        errbut.text="       登录成功        ";
                        errbut.Print();
                        User_ID=idinput.inputtext;
                        Sleep(100);
                        command=curpage.translate("home");
                    }
                }
            }
        }
    }while(command==0);
    return command;
};
int System::Sign_Up_Page(const Page& curpage){
    system("cls");
    Hide_Cursor();
    //主块
    Form mainform(5,10);
    Button freshbut(&mainform,string("@刷新页面"));
    Button backbut(&mainform,string("<返回上一级"),2);
    Button exitbut(&mainform,string("<退出系统 |"),19);
    Button titlebut(&mainform,string("用户注册页面"),0,70);
    mainform.Print();
    //注册表单
    Form signupform(15,70);
    Button_Box signupbut(&signupform,string("| 注册 |"),14,10);
    Text_Box nameinput(&signupform,string("  姓名：")+Print_Underline(15));
    Text_Box sexinput(&signupform,string("性别："),2);
    sexinput.inputtext="-1";
    Button malebut(&signupform,string("| 男 |"),2,7);
    Button femalebut(&signupform,string("| 女 |"),2,13);
    Text_Box idinput(&signupform,string("用户ID：")+Print_Underline(15),4);
    Text_Box teleinput(&signupform,string("  电话：")+Print_Underline(15),6);
    Text_Box emailinput(&signupform,string("  邮箱：")+Print_Underline(15),8);
    Text_Box pwdinput(&signupform,string("  密码：")+Print_Underline(15),10);
    Button errbut(&signupform,"",12);
    signupform.Print();

    int command=0,flag=0;
    pair<int,int> coord;
    do{
        flag=Get_Signal(coord);
        if(flag==2){

        }
        else if(flag==0){
            //老三样（刷新，返回，退出）
            if(Inside_Of(freshbut,coord)){
                Reverse_Color(freshbut);
                command = curpage.translate("fresh");
            }
            else if(Inside_Of(backbut,coord)){
                Reverse_Color(backbut);
                command = curpage.translate("back");
            }
            else if(Inside_Of(exitbut,coord)){
                Reverse_Color(exitbut);
                command = curpage.translate("exit");
            }
            //文本框输入
            else if(Inside_Of(malebut,coord)){
                Set_Color(1);
                malebut.Print();
                Set_Color(0);
                femalebut.Print();
                sexinput.inputtext="1";
            }
            else if(Inside_Of(femalebut,coord)){
                Set_Color(1);
                femalebut.Print();
                Set_Color(0);
                malebut.Print();
                sexinput.inputtext="0";
            }
            else if(Inside_Of(nameinput,coord)){
                nameinput.inputing();
            }
            else if(Inside_Of(idinput,coord)){
                idinput.inputing();
            }
            else if(Inside_Of(teleinput,coord)){
                teleinput.inputing();
            }
            else if(Inside_Of(emailinput,coord)){
                emailinput.inputing();
            }
            else if(Inside_Of(pwdinput,coord)){
                pwdinput.inputing();
            }
            //提交按钮，检查错误后若通过则写入数据文件，返回主页命令
            else if(Inside_Of(signupbut,coord)){
                Reverse_Color(signupbut);
                if(Legal_Name(nameinput.inputtext)==0){
                    errbut.text="   姓名格式错误,请重新输入   ";
                    errbut.Print();
                }
                else if(Legal_ID(idinput.inputtext)==0){
                    errbut.text="   用户ID格式错误,请重新输入   ";
                    errbut.Print();
                }
                else if(Unique_ID(idinput.inputtext)==0){
                    errbut.text="   用户ID已被使用,请重新输入   ";
                    errbut.Print();
                }
                else if(Legal_Tele(teleinput.inputtext)==0){
                    errbut.text="   电话号码格式错误,请重新输入   ";
                    errbut.Print();
                }
                else if(Unique_Tele(teleinput.inputtext)==0){
                    errbut.text="   电话号码已被使用,请重新输入   ";
                    errbut.Print();
                }
                else if(Legal_Email(emailinput.inputtext)==0){
                    errbut.text="   邮箱格式错误,请重新输入   ";
                    errbut.Print();
                }
                else if(Unique_Email(emailinput.inputtext)==0){
                    errbut.text="   邮箱已被使用,请重新输入   ";
                    errbut.Print();
                }
                else if(Legal_Pwd(pwdinput.inputtext)==0){
                    errbut.text="   密码格式错误,请重新输入   ";
                    errbut.Print();
                }
                else if(sexinput.inputtext=="-1"){
                    errbut.text="          请选择性别         ";
                    errbut.Print();
                }
                else{
                    errbut.text="           注册成功          ";
                    errbut.Print();
                    Sleep(1000);
                    Customer mnew(idinput.inputtext);
                    mnew.company="-1";
                    mnew.status=0;
                    mnew.name=nameinput.inputtext;
                    mnew.tele=teleinput.inputtext;
                    mnew.email=emailinput.inputtext;
                    mnew.pwd=pwdinput.inputtext;
                    mnew.sex=sexinput.inputtext[0]-'0';
                    mnew.Write();
                    command=curpage.translate("back");
                }
            }
        }
        else{
            
        }
    }while(command==0);
    return command;
};
int System::Forget_Page(const Page& curpage){
    Hide_Cursor();
    system("cls");
    Form mainform(5,5);
    Button freshbut(&mainform,string("@刷新页面"));
    Button backbut(&mainform,string("<返回"),4);
    Button exitbut(&mainform,string("<退出系统 |"),19);
    Button titlebut(&mainform,string("找回密码界面"),0,80);
    mainform.Print();
    
    pair<string,string> Vcode=back_Vcode(3);
    Form forgetform(10,70);
    Text_Box vcodeinput(&forgetform,string("验证码：")+Vcode.first+Print_Underline(5));
    Button altbut(&forgetform,string("| 刷新验证码 |"),0,vcodeinput.wid()+5);
    Text_Box idinput(&forgetform,string("请输入你的ID：")+Print_Underline(15),2);
    Button sendbut(&forgetform,string("| 发送验证码 |"),2,idinput.wid());
    Text_Box mailcodeinput(&forgetform,string("邮箱验证码：")+Print_Underline(4),4);
    Text_Box newpwdinput(&forgetform,string("新密码：")+Print_Underline(10),6);
    Button_Box commitbut(&forgetform,string("| 确认 |"),8,10);
    Button errbut(&forgetform,string(""),9);
    forgetform.Print();

    int command=0,flag=0,last_input=0,sendflag=0,vcodeflag=0,mailcodeflag=0;
    string str[10];
    string sendcode="";
    pair<int,int> coord;
    Customer u("");
    do{
        flag=Get_Signal(coord);
        if(flag==0)
        {
            if(Inside_Of(freshbut,coord)){
                Reverse_Color(freshbut);
                command = curpage.translate("fresh");
            }
            else if(Inside_Of(backbut,coord)){
                Reverse_Color(backbut);
                command = curpage.translate("back");
            }
            else if(Inside_Of(exitbut,coord)){
                Reverse_Color(exitbut);
                command = curpage.translate("exit");
            }
            else if(Inside_Of(vcodeinput,coord)){
                vcodeflag=0;
                vcodeinput.inputing();
                if(vcodeinput.inputtext!=Vcode.second){
                    errbut.text="        验证码错误          ";
                    errbut.Print();
                }
                else {
                    vcodeflag=1;
                }
            }
            else if(Inside_Of(altbut,coord)){
                Reverse_Color(altbut);
                vcodeflag=0;
                Vcode=back_Vcode(1);
                vcodeinput.guidetext=string("验证码：")+Vcode.first+Print_Underline(5);
                vcodeinput.Print();
                vcodeinput.inputtext="";
            }
            else if(Inside_Of(idinput,coord)){
                idinput.inputing();
            }
            else if(vcodeflag&&Inside_Of(sendbut,coord)){
                Reverse_Color(sendbut);
                if(Unique_ID(idinput.inputtext)==1){
                    errbut.text="     ID不存在,请重新输入   ";
                    errbut.Print();
                }
                else {
                    u.id=idinput.inputtext;
                    u.Complete();
                    sendcode=send_email(u.email);
                    sendflag=1;
                }
            }
            else if(sendflag&&Inside_Of(mailcodeinput,coord)){
                mailcodeinput.inputing();
                if(mailcodeinput.inputtext!=sendcode){
                    errbut.text="   邮箱验证码错误,请重新输入  ";
                    errbut.Print();
                }
                else {
                    mailcodeflag=1;
                }
            }
            else if(mailcodeflag&&Inside_Of(newpwdinput,coord)){
                newpwdinput.inputing();
            }
            else if(mailcodeflag&&Inside_Of(commitbut,coord)){
                Reverse_Color(commitbut);
                u.pwd=newpwdinput.inputtext;
                u.Modify("pwd");
                errbut.text="密码修改成功";
                errbut.Print();
                Sleep(2000);
                command=curpage.translate("back");
            }
        }
    }while(command==0);
    return command;
};
int System::Home_Page(const Page& curpage){
    Customer u(User_ID);
    u.Complete();
    Hide_Cursor();
    system("cls");
    Form mainform(3,10);
    Button freshbut(&mainform,string("@刷新页面"));
    Button backbut(&mainform,string("<退出登录"),freshbut.rely()+4);
    Button inforbut(&mainform,string("<个人信息 |"),backbut.rely()+2);
    Button historybut(&mainform,string("<历史订单 |"),inforbut.rely()+2);
    Button adminflibut(&mainform,string("<航班管理 |"),historybut.rely()+2);
    Button adminordbut(&mainform,string("<用户订单管理 |"),adminflibut.rely()+2);
    if(u.status==0){
        adminordbut.unable();
        adminflibut.unable();
    }
    Button exitbut(&mainform,string("<退出系统 |"),19);
    Button titlebut(&mainform,User_ID+string("的页面"),0,80);
    mainform.Print();
    Form searchform(7,75);
    Text_Box scityinput(&searchform,string("从")+Print_Underline(8));
    Text_Box ecityinput(&searchform,string("到")+Print_Underline(8),0,scityinput.wid());
    Button_Box searchbut(&searchform,string("| 搜索 |"),0,ecityinput.relx()+ecityinput.wid());
    Button errbut1(&searchform,string(""),0,searchbut.relx()+searchbut.wid());
    searchform.Print();

    Form timefilterform(9,75);
    Text_Box timeinput(&timefilterform,string("出发时间(hh:mm)最早为：")+Print_Underline(8));
    Button_Box timefilterbut(&timefilterform,string("| 过滤 |"),0,timeinput.wid());
    Button errbut2(&timefilterform,string(""),0,timefilterbut.relx()+ timefilterbut.wid());
    timefilterform.Print();

    Form payform(11,60);
    Text_Box numinput(&payform,string("输入航班号:")+Print_Underline(5));
    Text_Box seatinput(&payform,string(""));
    Button seatopt[3]={Button(&payform,string("| 经济舱 |"),0,20),Button(&payform,string("| 商务舱 |"),0,31),Button(&payform,string("| 头等舱 |"),0,42)};
    int opt1=0;
    Button_Box paybut(&payform,string("| 购买 |"),0,53);
    Button errbut3(&payform,string(""),0,paybut.relx()+paybut.wid());
    payform.Print();

    Form sortform(13,70);
    Button guidetext(&sortform,string("排序依据:"));
    Button sortby[3]={Button(&sortform,string("| 开始时间 |"),0,11),Button(&sortform,string("| 价格 |"),0,25),Button(&sortform,string("| 准点率 |"),0,35)};
    Button pribut(&sortform,string("| 重新输出 |"),0,45);
    Button errbut4(&sortform,string(""),0,pribut.relx()+pribut.wid());
    int opt2=0;
    sortform.Print();
    Set_Color(1);
    sortby[0].Print();
    Set_Color(0);

    int command=0,flag,cnt=0,inc=0,printflag=0;
    pair<int,int> coord;
    vector<Flight> allfli;
    Check_All_Flight(allfli);
    vector<Flight> nowfli;
    do{
        flag=Get_Signal(coord);
        if(flag==2){

        }
        else if(flag==0){
            if(Inside_Of(pribut,coord)){
                Reverse_Color(pribut);
                printflag=1;
            }
            else if(Inside_Of(scityinput,coord)){
                scityinput.inputing();
            }
            else if(Inside_Of(ecityinput,coord)){
                ecityinput.inputing();
            }
            else if(Inside_Of(adminflibut,coord)){
                Reverse_Color(adminflibut);
                command = curpage.translate("adminfli");
            }
            else if(Inside_Of(adminordbut,coord)){
                Reverse_Color(adminordbut);
                command = curpage.translate("adminord");
            }
            else if(Inside_Of(sortby[0],coord)){
                opt2=0;
                for(int i=0;i<3;++i){
                    if(i==opt2){
                        Set_Color(1);
                    }
                    sortby[i].Print();
                    Set_Color(0);
                }
            }
            else if(Inside_Of(sortby[1],coord)){
                opt2=1;
                for(int i=0;i<3;++i){
                    if(i==opt2){
                        Set_Color(1);
                    }
                    sortby[i].Print();
                    Set_Color(0);
                }
            }
            else if(Inside_Of(sortby[2],coord)){
                opt2=2;
                for(int i=0;i<3;++i){
                    if(i==opt2){
                        Set_Color(1);
                    }
                    sortby[i].Print();
                    Set_Color(0);
                }
            }
            else if(Inside_Of(searchbut,coord)){
                Reverse_Color(searchbut);
                nowfli=allfli;
                Filter_Based_On_City(nowfli,scityinput.inputtext,ecityinput.inputtext);
                if(nowfli.size()==0){
                    errbut1.text="没有从"+scityinput.inputtext+"到"+ecityinput.inputtext+"的航班";
                    errbut1.Print();
                }
                else {
                    printflag=1;
                }
            }
            else if(Inside_Of(timeinput,coord)){
                timeinput.inputing();
            }
            else if(Inside_Of(timefilterbut,coord)){
                Reverse_Color(timefilterbut);
                if(Legal_Time(timeinput.inputtext)){
                    Filter_Based_On_Stime(nowfli,timeinput.inputtext);
                    if(nowfli.size()){
                        printflag=1;
                    }
                    else {
                        errbut2.text="未查询到航班";
                        errbut2.Print();
                    }
                }
                else {
                    errbut2.text="时间格式不正确";
                    errbut2.Print();
                }
            }
            else if(Inside_Of(numinput,coord)){
                numinput.inputing();
                for(int i=0;i<3;++i){
                    if(i==opt1){
                        Set_Color(1);
                    }
                    seatopt[i].Print();
                    Set_Color(0);
                }
            }
            else if(Inside_Of(seatopt[0],coord)){
                opt1=0;
                for(int i=0;i<3;++i){
                    if(i==opt1){
                        Set_Color(1);
                    }
                    seatopt[i].Print();
                    Set_Color(0);
                }
            }
            else if(Inside_Of(seatopt[1],coord)){
                opt1=1;
                for(int i=0;i<3;++i){
                    if(i==opt1){
                        Set_Color(1);
                    }
                    seatopt[i].Print();
                    Set_Color(0);
                }
            }
            else if(Inside_Of(seatopt[2],coord)){
                opt1=2;
                for(int i=0;i<3;++i){
                    if(i==opt1){
                        Set_Color(1);
                    }
                    seatopt[i].Print();
                    Set_Color(0);
                }
            }
            else if(Inside_Of(paybut,coord)){
                Reverse_Color(paybut);
                if(Unique_Number(numinput.inputtext)){
                    errbut3.text="航班号不存在";
                    errbut3.Print();
                }
                else {
                    Flight fli(numinput.inputtext);
                    fli.Complete();
                    if(fli.rem[opt1]==0){
                        errbut3.text="抱歉，查询的舱位已售罄";
                        errbut3.Print();
                    }
                    else {
                        F_num=fli.num;
                        Seat=opt1;
                        command=curpage.translate("buy");
                    }
                }
            }
            else if(Inside_Of(freshbut,coord)){
                Reverse_Color(freshbut);
                command = curpage.translate("fresh");
            }
            else if(Inside_Of(backbut,coord)){
                Reverse_Color(backbut);
                command = curpage.translate("back");
            }
            else if(Inside_Of(inforbut,coord)){
                Reverse_Color(inforbut);
                command = curpage.translate("infor");
            }
            else if(Inside_Of(historybut,coord)){
                Reverse_Color(historybut);
                command = curpage.translate("order");
            }
            else if(Inside_Of(exitbut,coord)){
                Reverse_Color(exitbut);
                command = curpage.translate("exit");
            }
        }
        if(printflag==1){ 
            for(int i=1;i<=cnt;++i){
                Form spaceform(17+(i-1)*inc,85);
                for(int j=1;j<=inc;++j){
                    Button spacebut(&spaceform,"                                              ",j-1);
                }
                spaceform.Print();
            }
            cnt=0;
            inc=0;
            if(nowfli.size()==0){
                errbut4.text="未查询到航班";
                errbut4.Print();
            }
            else{
                if(opt2==0){
                    nowfli=Flight_Sort(nowfli,"stime");
                }
                else if(opt2==1){
                    nowfli=Flight_Sort(nowfli,"prices");
                }
                else if(opt2==2){
                    nowfli=Flight_Sort(nowfli,"rate");
                }
                for(auto fli:nowfli){
                    Form output(17+inc*cnt,85);
                    cnt++;
                    inc=fli.Display(output);
                    output.Print();
                }
            }
            printflag=0;
        }
        
    }while(command==0);
    return command;
}
int System::Infor_Page(const Page& curpage){
    
    string modtext="| 修改 |";
    string committext="| 确认 |";
    Customer u(User_ID);
    u.Complete();
    Hide_Cursor();
    system("cls");
    Form mainform(5,5);
    Button freshbut(&mainform,string("@刷新页面"));
    Button backbut(&mainform,string("<返回主页"),freshbut.rely()+4);
    Button exitbut(&mainform,string("<退出系统 |"),19);
    Button titlebut(&mainform,User_ID+string("的个人信息面板"),0,80);
    Button errbut(&mainform,string(""));
    mainform.Print();

    Form nameform(5,60);
    Button_Box namemodify(&nameform,modtext);
    Button nameshow(&nameform,string("姓名:")+u.name,0,modtext.length());
    Text_Box nameinput(&nameform,string("                   "),nameshow.rely()+1,modtext.length());
    nameform.Print();
    int nameflag=0;

    Form sexform(7,60);
    Button sexshow(&sexform,string("性别:")+Sex_In_Chinese(u.sex),0,modtext.length());
    sexform.Print();

    Form idform(9,60);
    Button idshow(&idform,string("用户ID:")+u.id,0,modtext.length());
    idform.Print();

    Form teleform(11,60);
    Button_Box telemodify(&teleform,modtext);
    Button teleshow(&teleform,string("电话:")+u.tele,0,modtext.length());
    Text_Box teleinput(&teleform,string("                   "),teleshow.rely()+1,modtext.length());
    teleform.Print();
    int teleflag=0;

    Form emailform(13,60);
    Button_Box emailmodify(&emailform,modtext);
    Button emailshow(&emailform,string("邮箱:")+u.email,0,modtext.length());
    Text_Box emailinput(&emailform,string("                   "),nameshow.rely()+1,modtext.length());
    emailform.Print();
    int emailflag=0;

    Form statusform(15,55);                     
    Button_Box statusmodify(&statusform,string("               "));
    if(u.status==0)statusmodify.text="| 管理员认证 |";
    Button statusshow(&statusform,string("身份:")+Status_To_Chinese(u.status),0,statusmodify.wid());
    statusform.Print();

    Form comform(17,60);
    Button comshow(&comform,string("公司:")+u.company,0,modtext.length());
    Text_Box cominput(&comform,string("                   "),comshow.rely()+1,modtext.length());
    if(u.status==1)comform.Print();

    int command=0,flag,last_input=0;
    string str;
    pair<int,int> coord;
    do{
        flag=Get_Signal(coord);
        if(flag==0){
            if(Inside_Of(freshbut,coord)){
                Reverse_Color(freshbut);
                command = curpage.translate("fresh");
            }
            else if(Inside_Of(backbut,coord)){
                Reverse_Color(backbut);
                command = curpage.translate("back");
            }
            else if(Inside_Of(exitbut,coord)){
                Reverse_Color(exitbut);
                command = curpage.translate("exit");
            }
            else if(u.status==0&&Inside_Of(statusmodify,coord)){
                Reverse_Color(statusmodify);
                command= curpage.translate("authen");
            }
            else if(nameflag==0&&Inside_Of(namemodify,coord)){
                Reverse_Color(namemodify);
                namemodify.text=committext;
                namemodify.Print();
                nameflag=1;
                nameinput.guidetext="新姓名:"+Print_Underline(8);
                nameinput.inputing();
            }
            else if(nameflag==1){
                if(Inside_Of(nameinput,coord)){
                    nameinput.inputing();
                }
                else if(Inside_Of(namemodify,coord)){
                    Reverse_Color(namemodify);
                    namemodify.text=modtext;
                    namemodify.Print();
                    nameflag=0;
                    u.name=nameinput.inputtext;
                    u.Modify("name");
                    command=curpage.translate("fresh");
                }
            }
            else if(teleflag==0&&Inside_Of(telemodify,coord)){
                Reverse_Color(telemodify);
                telemodify.text=committext;
                telemodify.Print();
                teleflag=1;
                teleinput.guidetext="新电话:"+Print_Underline(8);
                teleinput.inputing();
            }
            else if(teleflag==1){
                if(Inside_Of(teleinput,coord)){
                    teleinput.inputing();
                }
                else if(Inside_Of(telemodify,coord)){
                    Reverse_Color(telemodify);
                    telemodify.text=modtext;
                    telemodify.Print();
                    teleflag=0;
                    if(Legal_Tele(teleinput.inputtext)==0){
                        errbut.text="电话格式错误，请重新输入";
                        errbut.Print();
                    }
                    else if(Unique_Tele(teleinput.inputtext)){
                        u.tele=teleinput.inputtext;
                        u.Modify("tele");
                        command=curpage.translate("fresh");
                    }
                    else{
                        errbut.text="电话已被使用，请重新输入";
                        errbut.Print();
                    }
                }
            }
            else if(emailflag==0&&Inside_Of(emailmodify,coord)){
                Reverse_Color(emailmodify);
                emailmodify.text=committext;
                emailmodify.Print();
                emailflag=1;
                emailinput.guidetext="新邮箱:"+Print_Underline(8);
                emailinput.inputing();
            }
            else if(emailflag==1){
                if(Inside_Of(emailinput,coord)){
                    emailinput.inputing();
                }
                else if(Inside_Of(emailmodify,coord)){
                    Reverse_Color(emailmodify);
                    emailmodify.text=modtext;
                    emailmodify.Print();
                    emailflag=0;
                    if(Legal_Email(emailinput.inputtext)==0){
                        errbut.text="邮件格式错误，请重新输入";
                        errbut.Print();
                    }
                    else if(Unique_Email(emailinput.inputtext)){
                        u.email=emailinput.inputtext;
                        u.Modify("email");
                        command=curpage.translate("fresh");
                    }
                    else{
                        errbut.text="邮件已被使用，请重新输入";
                        errbut.Print();
                    }
                }
            }
        }
    }while(command==0);
    return command;
};
int System::Order_Page(const Page& curpage){
    Hide_Cursor();
    system("cls");
    Form mainform(0,0);
    Button freshbut(&mainform,string("@刷新页面"));
    Button backbut(&mainform,string("<回到主页"),freshbut.rely()+4);
    Button exitbut(&mainform,string("<退出系统 |"),19);
    Button titlebut(&mainform,User_ID+string("的历史订单"),0,100);
    mainform.Print();

    int command=0;
    pair<int,int> coord;
    int flag,inc=0;
    
    vector<Order> allord;
    Check_All_Order(allord);
    vector<Order> myord=allord;
    Filter_Based_On_ID(myord,User_ID);
    myord=Order_Sort(myord,"time");
    int cnt=0;//订单数
    for(auto ord:myord){
        Form output(13+inc*cnt,70);
        cnt++;
        inc=ord.Display(output);
        output.Print();
    }
    do{
        flag=Get_Signal(coord);
        if(flag==0){
            if(Inside_Of(backbut,coord)){
                Reverse_Color(backbut);
                command=curpage.translate("back");
            }
            else if(Inside_Of(freshbut,coord)){
                Reverse_Color(freshbut);
                command=curpage.translate("fresh");
            }
            else if(Inside_Of(exitbut,coord)){
                Reverse_Color(exitbut);
                command=curpage.translate("exit");
            }
        }
    }while(command==0);
    return command;
}
int System::Buy_Page(const Page& curpage){
    Hide_Cursor();
    system("cls");
    Form mainform(5,10);
    Button freshbut(&mainform,string("@刷新页面"));
    Button backbut(&mainform,string("<取消订单"),freshbut.rely()+4);
    Button exitbut(&mainform,string("<退出系统 |"),19);
    Button titlebut(&mainform,string("订单生成页面"),0,80);
    mainform.Print();

    Customer u(User_ID);
    Flight fli(F_num);
    Order ord;
    u.Complete();
    fli.Complete();
    ord.tim=Get_Time();
    ord.name=u.name;
    ord.num=fli.num;
    ord.cit[0]=fli.cit[0];
    ord.cit[1]=fli.cit[1];
    ord.pri=fli.pri[Seat];
    ord.stime=fli.stime;
    ord.etime=fli.etime;
    ord.seat=Seat;
    ord.com=fli.com;
    Form idform(20,70);
    Text_Box idinput(&idform,string("使用人ID：")+Print_Underline(10));
    idform.Print();
    Form orderform(22,70);
    ord.Display(orderform);
    Button paybut(&orderform,"| 购买 |",7);
    paybut.unable();
    Button errbut(&orderform,"",8);
    orderform.Print();


    int command=0,flag=0;
    pair<int,int> coord;
    do{
        flag=Get_Signal(coord);
        if(flag==0){
            if(Inside_Of(backbut,coord)){
                Reverse_Color(backbut);
                command=curpage.translate("back");
            }
            else if(Inside_Of(freshbut,coord)){
                Reverse_Color(freshbut);
                command=curpage.translate("fresh");
            }
            else if(Inside_Of(exitbut,coord)){
                Reverse_Color(exitbut);
                command=curpage.translate("exit");
            }
            else if(Inside_Of(idinput,coord)){
                idinput.inputing();
                if(Unique_ID(idinput.inputtext)){
                    errbut.text="该用户未在航空售票系统注册，请重新输入或注册";
                    errbut.Print();
                }
                else{
                    ord.id=idinput.inputtext;
                    if(Legal_Order(ord,ord.id)==0){
                        errbut.text="订单与该用户所购买订单冲突，请检查历史订单或重选航班";
                        errbut.Print();
                        Sleep(2000);
                    }
                    else{
                        paybut.able();
                    }
                }
            }
            else if(Inside_Of(paybut,coord)){
                Reverse_Color(paybut);
                ord.Write();
                fli.bought(ord.seat);
                errbut.text="购买成功";
                errbut.Print();
                Sleep(2000);
                command=curpage.translate("back");
            }
        }
        
    }while(command==0);
    return command;
}
int System::Authen_Page(const Page& curpage){
    Hide_Cursor();
    system("cls");
    Form mainform(5,10);
    Button titlebut(&mainform,string("管理员认证页面"),0,70);
    Button backbut(&mainform,string("<个人信息 |"),2);
    Button freshbut(&mainform,string("@刷新页面"));
    Button exitbut(&mainform,string("<退出系统 |"),19);
    mainform.Print();

    Form companyform(10,80);
    Text_Box companyinput(&companyform,string("公司名:")+Print_Underline(8));
    Button_Box commitbut(&companyform,string("| 认证 |"),2,5);
    Button errbut(&companyform,string(""),4);
    companyform.Print();

    pair<int,int> coord;
    int flag,command=0;
    do{
        flag=Get_Signal(coord);
        if(flag==0){
            if(Inside_Of(backbut,coord)){
                Reverse_Color(backbut);
                command = curpage.translate("back");

            }
            else if(Inside_Of(freshbut,coord)){
                Reverse_Color(freshbut);
                command = curpage.translate("fresh");
            }
            else if(Inside_Of(exitbut,coord)){
                Reverse_Color(exitbut);
                command = curpage.translate("exit");
            }
            else if(Inside_Of(companyinput,coord)){
                companyinput.inputing();
            }
            else if(Inside_Of(commitbut,coord)){
                Reverse_Color(commitbut);
                if(Unique_Company_Name(companyinput.inputtext)){
                    errbut.text="认证成功";
                    errbut.Print();
                    Customer u(User_ID);
                    u.Complete();
                    u.status=1;
                    u.company=companyinput.inputtext;
                    u.Modify("company");
                    u.Modify("status");
                    command = curpage.translate("back");
                }
                else {
                    errbut.text="该公司已有管理员，请重新输入";
                    errbut.Print();
                }
            }
        }
    }while(command==0);

   return command;
}

int System::Adminfli_Page(const Page& curpage){
    Customer u(User_ID);
    u.Complete();
    Hide_Cursor();
    system("cls");

    Form mainform(5,10);
    Button titlebut(&mainform,string("航班管理页面"),0,80);
    Button backbut(&mainform,string("<返回主页 |"),2);
    Button freshbut(&mainform,string("@刷新页面"));
    Button exitbut(&mainform,string("<退出系统 |"),19);
    Button addflibut(&mainform,string("<发布新航班 |"),4);
    mainform.Print();

    int command=0,flag,cnt=0,inc=0,printflag=1;
    vector<Flight> allfli;
    Check_All_Flight(allfli);
    vector<Flight> nowfli=allfli;
    Filter_Based_On_Company(nowfli,u.company);
    nowfli=Flight_Sort(nowfli,"stime");
    pair<int,int> coord;
    if(nowfli.size()){
        if(printflag==1){ 
            for(int i=1;i<=cnt;++i){
                Form spaceform(13+(i-1)*inc,70);
                for(int j=1;j<=inc;++j){
                    Button spacebut(&spaceform,"                                              ",j-1);
                }
                spaceform.Print();
            }
            cnt=0;
            inc=0;
            
            for(auto fli:nowfli){
                Form output(13+inc*cnt,70);
                inc=fli.Display(output,"111111");
                output.Print();
                cnt++;
            }
            printflag=0;
        }
    }
    else {
        curmove(15,80);
        cout<<string("未查询到")+u.company+string("的信息");
    }
    do{
        flag=Get_Signal(coord);
        if(flag==0){
            if(Inside_Of(backbut,coord)){
                Reverse_Color(backbut);
                command = curpage.translate("back");
            }
            else if(Inside_Of(freshbut,coord)){
                Reverse_Color(freshbut);
                command = curpage.translate("fresh");
            }
            else if(Inside_Of(exitbut,coord)){
                Reverse_Color(exitbut);
                command = curpage.translate("exit");
            }
            else if(Inside_Of(addflibut,coord)){
                Reverse_Color(addflibut);
                command = curpage.translate("addfli");
            }
        }
    }while(command==0);
    return command;
}
int System::Addfli_Page(const Page& curpage){
    Hide_Cursor();
    system("cls");
    Form mainform(5,10);
    Button titlebut(&mainform,string("航班添加页面"),0,80);
    Button backbut(&mainform,string("<返回航班管理 |"),2);
    Button freshbut(&mainform,string("@刷新页面"));
    Button exitbut(&mainform,string("<退出系统 |"),19);
    mainform.Print();

    Form addform(15,70);
    Button_Box commitbut(&addform,string("| 发布 |"),30);
    Text_Box numinput(&addform,string("航班号:")+Print_Underline(5));
    Text_Box scityinput(&addform,string("从")+Print_Underline(5),2);
    Text_Box ecityinput(&addform,string("飞往")+Print_Underline(5),4);
    Text_Box stimeinput(&addform,string("起飞时间:")+Print_Underline(5),6);
    Text_Box etimeinput(&addform,string("到达时间:")+Print_Underline(5),8);
    Text_Box modelinput(&addform,string("机型:")+Print_Underline(5),10);
    Text_Box priinput1(&addform,string("经济舱票价:")+Print_Underline(5),12);
    Text_Box priinput2(&addform,string("商务舱票价:")+Print_Underline(5),14);
    Text_Box priinput3(&addform,string("头等舱票价:")+Print_Underline(5),16);
    Text_Box reminput1(&addform,string("经济舱座位数:")+Print_Underline(5),18);
    Text_Box reminput2(&addform,string("商务舱座位数:")+Print_Underline(5),20);
    Text_Box reminput3(&addform,string("头等舱座位数:")+Print_Underline(5),22);
    Text_Box pilinput1(&addform,string("主飞行员:")+Print_Underline(5),24);
    Text_Box pilinput2(&addform,string("副飞行员:")+Print_Underline(5),26);
    Text_Box rateinput(&addform,string("准点率:")+Print_Underline(5),28);
    Button errbut(&addform,string(""),32);
    addform.Print();

    int command=0,flag;
    pair<int,int> coord;
    do{
        flag=Get_Signal(coord);
        if(flag==0){
            if(Inside_Of(backbut,coord)){
                Reverse_Color(backbut);
                command=curpage.translate("back");
            }
            else if(Inside_Of(freshbut,coord)){
                Reverse_Color(freshbut);
                command=curpage.translate("fresh");
            }
            else if(Inside_Of(exitbut,coord)){
                Reverse_Color(exitbut);
                command=curpage.translate("exit");
            }
            else if(Inside_Of(commitbut,coord)){
                if(Legal_Number(numinput.inputtext)==0){
                    errbut.text="航班号格式错误";
                    errbut.Print();
                }
                else if(Unique_Number(numinput.inputtext)==0){
                    errbut.text="已有相同航班号";
                    errbut.Print();
                }
                else if(Legal_Time(stimeinput.inputtext)==0||Legal_Time(stimeinput.inputtext)==0){
                    errbut.text="时间格式错误";
                    errbut.Print();
                }
                else if(Legal_Digit(priinput1.inputtext)==0||
                        Legal_Digit(priinput2.inputtext)==0||
                        Legal_Digit(priinput3.inputtext)==0){
                    errbut.text="价格格式错误";
                    errbut.Print();
                }
                else if(Legal_Digit(reminput1.inputtext)==0||
                        Legal_Digit(reminput2.inputtext)==0||
                        Legal_Digit(reminput3.inputtext)==0){
                    errbut.text="舱位格式错误";
                    errbut.Print();
                }
                else if(Legal_Digit(rateinput.inputtext)==0){
                    errbut.text="准点率格式错误";
                    errbut.Print();
                }
                else {
                    Flight f(numinput.inputtext);
                    Customer u(User_ID);
                    u.Complete();
                    f.com=u.company;
                    f.num=numinput.inputtext;
                    f.cit[0]=scityinput.inputtext;
                    f.cit[1]=ecityinput.inputtext;
                    f.stime=stimeinput.inputtext;
                    f.etime=etimeinput.inputtext;
                    f.mod=modelinput.inputtext;
                    f.pil[0]=pilinput1.inputtext;
                    f.pil[1]=pilinput2.inputtext;
                    stringstream strout;
                    strout.str(priinput1.inputtext);
                    strout>>f.pri[0];
                    strout.clear();
                    strout.str(priinput2.inputtext);
                    strout>>f.pri[1];
                    strout.clear();
                    strout.str(priinput3.inputtext);
                    strout>>f.pri[2];
                    strout.clear();
                    strout.str(reminput1.inputtext);
                    strout>>f.rem[0];
                    strout.clear();
                    strout.str(reminput2.inputtext);
                    strout>>f.rem[1];
                    strout.clear();
                    strout.str(reminput3.inputtext);
                    strout>>f.rem[2];
                    strout.clear();
                    strout.str(rateinput.inputtext);
                    strout>>f.rat;
                    strout.clear();
                    f.Write();
                    errbut.text="发布成功";
                    errbut.Print();
                    Sleep(1000);
                    command = curpage.translate("back");
                }
            }
            else if(Inside_Of(numinput,coord)){
                numinput.inputing();
            }
            else if(Inside_Of(scityinput,coord)){
                scityinput.inputing();
            }
            else if(Inside_Of(ecityinput,coord)){
                ecityinput.inputing();
            }
            else if(Inside_Of(etimeinput,coord)){
                etimeinput.inputing();
            }
            else if(Inside_Of(stimeinput,coord)){
                stimeinput.inputing();
            }
            else if(Inside_Of(modelinput,coord)){
                modelinput.inputing();
            }
            else if(Inside_Of(priinput1,coord)){
                priinput1.inputing();
            }
            else if(Inside_Of(priinput2,coord)){
                priinput2.inputing();
            }
            else if(Inside_Of(priinput3,coord)){
                priinput3.inputing();
            }
            else if(Inside_Of(reminput1,coord)){
                reminput1.inputing();
            }
            else if(Inside_Of(reminput2,coord)){
                reminput2.inputing();
            }
            else if(Inside_Of(reminput3,coord)){
                reminput3.inputing();
            }
            else if(Inside_Of(pilinput1,coord)){
                pilinput1.inputing();
            }
            else if(Inside_Of(pilinput2,coord)){
                pilinput2.inputing();
            }
            else if(Inside_Of(rateinput,coord)){
                rateinput.inputing();
            }
        }
    }while(command==0);
    return command;
}
int System::Adminord_Page(const Page& curpage){
    Hide_Cursor();
    system("cls");
    Form mainform(5,10);
    Button titlebut(&mainform,string("订单管理页面"),0,80);
    Button freshbut(&mainform,string("@刷新页面"));
    Button backbut(&mainform,string("<返回主页"),2);
    Button exitbut(&mainform,string("<退出系统"),19);
    mainform.Print();

    Customer u(User_ID);
    u.Complete();
    vector<Order> allord;
    Check_All_Order(allord);
    vector<Order> myord=allord;
    Filter_Based_On_Company(myord,u.company);
    int inc=0,cnt=0;
    for(auto ord:myord){
        Form output(13+inc*cnt,70);
        inc=ord.Display(output);
        output.Print();
        cnt++;
    }
    int command=0,flag=0;
    pair<int,int> coord;
    do{
        flag=Get_Signal(coord);
        if(flag==0){
            if(Inside_Of(freshbut,coord)){
                Reverse_Color(freshbut);
                command = curpage.translate("fresh");
            }
            else if(Inside_Of(backbut,coord)){
                Reverse_Color(backbut);
                command = curpage.translate("back");
            }
            else if(Inside_Of(exitbut,coord)){
                Reverse_Color(exitbut);
                command = curpage.translate("exit");
            }

        }
    }while(command==0);
    return command;
}