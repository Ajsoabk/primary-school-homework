#include<vector>
#include<string>
#include<stack>
#include<map>
using namespace std;
class System;
class Page;
class Button;
class Button_Box;
class Text_Box;
class Order;
class Form;
bool Legal_ID(const string&);
bool Legal_Name(const string&);
bool Legal_Pwd(const string&);
bool Legal_Tele(const string&);
bool Legal_Email(const string&);
bool Legal_Time(const string& );

bool Unique_ID(const string&);
bool Unique_Tele(const string&);
bool Unique_Email(const string&);

string Check_By_ID(const string& );//根据ID返回密码

bool Legal_Company_Name(const string &);
bool Unique_Company_Name(const string &);
class Customer{
    private:
    public:
        int status;
        string id;
        string name;
        string pwd;
        bool sex;
        string tele;
        string email;
        string company;
        void Write();
        void Modify(string mod="all");
        int Complete();
        Customer (string str){
            id=str;
        }
};
Customer Get_Customer_Infor(string);
class Flight{
    public:
        string num;
        string com;
        string cit[2];
        string mod;
        //机型
        string pil[2];
        string stime;
        //出发时间
        string etime;
        //到达时间
        int rem[3];
        //0为经济舱，1为商务舱，2为头等舱
        int pri[3];
        int rat;
        //准点率

        int Display(Form&,string command="1111111");
        //显示航班信息
        void Write();
        Flight(string flightnum){
            num=flightnum;
        }
        void Complete();
        void bought(const int& );
        void Modify(string mod="all");
};
bool Legal_Number(const string& );
bool Unique_Number(const string &);
bool Legal_Digit(const string& );

//获取数据库中所有航班信息
void Check_All_Flight(vector<Flight> &);

//航班信息过滤
void Filter_Based_On_Company(vector<Flight>&,string,bool mode=0);//mode为0时表示只保留该company的航班，为1时表示把该company的航班滤去
void Filter_Based_On_City(vector<Flight>&,string,string);
void Filter_Based_On_Stime(vector<Flight>&,string stime,bool mode=0);//mode为0时表示只保留起飞时间在mtime之后的航班,为1则相反
void Filter_Based_On_Etime(vector<Flight>&,string etime,bool mode=0);//mode为0时表示只保留到达时间在mtime之前的航班,为1则相反，总之0都是正常需要，1都是特殊的相反需要
void Filter_Based_On_Time(vector<Flight>&,string stime,string etime);
//航班信息排序
/*
command有
"stime":根据出发时间顺序排列
"etime":根据到达时间顺序排列
"prices":根据最低价格（prices[0]）从低到高
*/
vector<Flight> Flight_Sort(vector<Flight>,string command);
class Page{
    public:
        //把用户在当前页面的字符串类型的输入映射到整数类型的命令
        map<string,int> options;
        //把整数类型的命令映射到指向对应页面的指针
        map<int,Page*> optionsPtr;
        //指向负责渲染当前页面的函数的指针
        int (*funcPtr)(const Page&);
        int translate(string)const ;
        Page();
        //各页面建立联系
        void Ini_Welcome();
        void Ini_Log_In();
        void Ini_Sign_Up();
        void Ini_Forget();
        void Ini_Home();
        void Ini_Infor();
        void Ini_Order();
        void Ini_Authen();
        void Ini_Buy();
        void Ini_Addfli();
        void Ini_Adminfli();
        void Ini_Adminord();
        
        Button *Hover_In_Which();//返回鼠标悬停在当前页面的哪个块
        Button *Click_In_Which();//返回鼠标在当前页面的哪个块点击了
        vector<Button> Get_All_Button();
};
//各页面的渲染代码
class System{
    public:
        static string User_ID;
        static string F_num;
        static int Seat;
        //系统初始化
        Page page;
        stack<Page> sta;
        static int Welcome_Page(const Page&);
        static int Log_In_Page(const Page&);
        static int Sign_Up_Page(const Page&);
        static int Forget_Page(const Page&);
        static int Home_Page(const Page&);
        static int Search_Page(const Page&);
        static int Infor_Page(const Page&);
        static int Order_Page(const Page&);
        static int Buy_Page(const Page&);
        static int Authen_Page(const Page&);
        static int Modify_Page(const Page&);
        static int Addfli_Page(const Page&);
        static int Adminfli_Page(const Page&);
        static int Adminord_Page(const Page&);
        System();
};
class Order{
    public:
        string id;
        string num;
        int seat;
        int payed;
        string name;
        string cit[2];
        int pri;
        string stime;
        string com;
        string etime;
        string tim;
        void Write();
        int Display(Form &);
};
void Check_All_Order(vector<Order> &);
void Filter_Based_On_ID(vector<Order>&,string);
void Filter_Based_On_Company(vector<Order>&,string);
vector<Order> Order_Sort(vector<Order>,string);
bool Legal_Order(const Order &,string );
/*
表单类，主要功能是辅助定位
*/
class Form{
    private:
        pair<int,int> abscoor;
    public:
    Button_Box *commitbut;
    vector<Text_Box> input;
    vector<Button> singlebut;

    int y()const{return abscoor.first;}
    int x()const{return abscoor.second;}
    void Print()const;
    Form(int absy,int absx){
        abscoor.first=absy;
        abscoor.second=absx;
        commitbut=nullptr;
    }
};
/*
文本框类，主要功能是获取输入及储存输入
*/
class Text_Box{
    private:
        pair<int,int> abscoor;
        pair<int,int> relcoor;
        int width;
    public:
        string inputtext;//用户的输入
        string guidetext;//引导语
        int y()const{return abscoor.first;};
        int x()const{return abscoor.second;};
        int rely()const{return relcoor.first;};
        int relx()const{return relcoor.second;};
        int wid()const{return width;};
        void inputing();
        void Print()const;
        Text_Box(Form *f,string t,int rely=0,int relx=0){
            relcoor.first=rely;
            relcoor.second=relx;
            //计算绝对坐标
            abscoor.first=f->y()+rely;
            abscoor.second=f->x()+relx;

            guidetext=t;
            width=t.length();
            f->input.push_back(*this);
        }
};
/*
表单提交按钮类
*/
class Button_Box{
    private:
        pair<int,int> abscoor;
        pair<int,int> relcoor;
        int width;
        Form* inwhichform;
    public:
        string text;
        Form* form(){return inwhichform;};
        int y()const{return abscoor.first;};
        int x()const{return abscoor.second;};
        int rely()const{return relcoor.first;};
        int relx()const{return relcoor.second;};
        int wid()const{return width;};
        void Print()const;
        Button_Box(Form* f,string t,int rely=0,int relx=0){
            relcoor.first=rely;
            relcoor.second=relx;
            abscoor.first=f->y()+rely;
            abscoor.second=f->x()+relx;
            text=t;
            inwhichform=f;
            width=t.length();
            f->commitbut=this;
        }
};
/*
简单按钮类
*/
class Button{
    private:
        pair<int,int> abscoor;
        pair<int,int> relcoor;
        int width;
        bool ablity;
    public:
        string text;
        int y()const{return abscoor.first;};
        int x()const{return abscoor.second;};
        int rely()const{return relcoor.first;};
        int relx()const{return relcoor.second;};
        int wid()const{return width;};
        void able(){ablity=1;};
        void unable(){ablity=0;};
        bool clickable()const{return ablity;};
        void Print()const;
        Button(Form*f,string t,int rely=0,int relx=0){
            relcoor.first=rely;
            relcoor.second=relx;
            abscoor.first=f->y()+rely;
            abscoor.second=f->x()+relx;
            text=t;
            width=t.length();
            ablity=1;
            f->singlebut.push_back(*this);
        }
};
//ATSStructure文件的函数
//ATSGraph文件的函数
void full_screen();
bool Inside_Of(const Button&,pair<int,int>);
bool Inside_Of(const Button_Box&,pair<int,int>);
bool Inside_Of(const Text_Box&,pair<int,int>);
void Unable_Quick_Edit_Mode();
void curmove(const short y,const short x);
bool Click_On_It(const Button&,int,int);
pair<int,int> Get_Click_Pos_Of_Client(int );
pair<int,int> Get_One_Click_Pos();
int Get_Signal(pair<int,int> &);
int Get_Center_Position(string );
int Get_Center_Position(int );
void Set_Color(int );
void Hide_Cursor(bool flag=true);
string Print_In_Center(string);
bool Move_To_Next_Line(short );
void Reverse_Color(const Button &);
void Reverse_Color(const Button_Box &);
bool Clicked(int );
//鼠标位置

string Get_Time();