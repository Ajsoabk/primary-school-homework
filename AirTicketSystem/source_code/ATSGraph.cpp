#include<windows.h>
#include"ATS.h"
#include<sstream>
#include<iostream>
#include<string>
#include<conio.h>
#include<map>
using std::pair;
using std::string;
using std::ostringstream;
using std::cout;
using std::make_pair;

bool Inside_Of(const Button& but,pair<int,int> coor){//检查该坐标是否在按钮范围内
    if(but.clickable()==0)return false;
    const int &u=but.y();
    int b=but.y();
    const int &l=but.x();
    int r=but.x()+but.wid();
    return coor.first>=u&&coor.first<=b&&coor.second>=l&&coor.second<=r;
};
bool Inside_Of(const Text_Box& but,pair<int,int> coor){//检查该坐标是否在按钮范围内
    const int &u=but.y();
    int b=but.y();
    const int &l=but.x();
    int r=but.x()+but.wid();
    return coor.first>=u&&coor.first<=b&&coor.second>=l&&coor.second<=r;
};
bool Inside_Of(const Button_Box& but,pair<int,int> coor){//检查该坐标是否在按钮范围内
    const int &u=but.y();
    int b=but.y();
    const int &l=but.x();
    int r=but.x()+but.wid();
    return coor.first>=u&&coor.first<=b&&coor.second>=l&&coor.second<=r;
};
/*
光标隐藏函数
传入是否隐藏的bool参数
*/
void Hide_Cursor(bool flag)    //定义函数
{ 
    HANDLE hout=GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_CURSOR_INFO cursor; //定义结构体 控制台光标信息
    cursor.bVisible = (!flag); //设置不显示n
    cursor.dwSize = 1; 
    SetConsoleCursorInfo(hout,&cursor); //设置光标信息
}
void full_screen(){//全屏显示
    HWND froWindow=GetForegroundWindow();
    int cx=GetSystemMetrics(SM_CXSCREEN);
    int cy=GetSystemMetrics(SM_CYSCREEN);

    LONG froWindowInfor=GetWindowLong(froWindow,GWL_STYLE);
    SetWindowLong(froWindow,GWL_STYLE,(froWindowInfor|WS_POPUP|WS_MAXIMIZE)& ~WS_CAPTION& ~WS_THICKFRAME& ~WS_BORDER);
    SetWindowPos(froWindow,HWND_TOP,0,0,cx,cy,0);
};
void curmove(const short y,const short x)//定位到y行x列
{
    HANDLE hOut; //新建句柄 hOut
    hOut=GetStdHandle(STD_OUTPUT_HANDLE);  //实例化句柄 hOut
    COORD pos={x,y};
    SetConsoleCursorPosition(hOut,pos);
};
void Unable_Quick_Edit_Mode(){//取消快速编辑模式
    HANDLE stdwin=GetStdHandle(STD_INPUT_HANDLE);//获取输出窗口句柄
    DWORD mode;
    GetConsoleMode(stdwin,&mode);//获取该窗口的模式信息
    mode &= ~ENABLE_QUICK_EDIT_MODE;//在模式信息中取消掉快速编辑模式
    SetConsoleMode(stdwin,mode);
};
void Set_Color(int fla=0)//输入1白底黑字，0黑底白字
{
    if(fla){
        fla=0x0070;
    }
    else fla=0x0007;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),fla);
}
inline int Get_Center_Position(const int len){//获取让字符串在屏幕中间需要打的空格数
	int One_wid=GetSystemMetrics(SM_CXCURSOR);
	int wid=GetSystemMetrics( SM_CXFULLSCREEN);
	wid=wid*4/One_wid;
    return (wid-len)/2;
}
int Get_Center_Position(string str){//获取让字符串在屏幕中间需要打的空格数
    return Get_Center_Position(str.length());
}
bool Move_To_Next_Line(short n=1)//光标移动到下一行
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	if (GetConsoleScreenBufferInfo(hConsole, &csbi)&&csbi.dwCursorPosition.Y+n>=0)
	{
        HANDLE hOut; 
        hOut=GetStdHandle(STD_OUTPUT_HANDLE);  //实例化句柄 hOut
        COORD pos={0,0};
        pos.Y=csbi.dwCursorPosition.Y+n;
        SetConsoleCursorPosition(hOut,pos);
        return true;//移动成功
	}
    return false;//移动失败
}
string Print_In_Center(string str){
    
	int pos=Get_Center_Position(str);
    ostringstream strout;
	for(int i=0;i<pos;++i){
        strout<<" ";
	}
    strout<<str;
    return strout.str();
}
bool Clicked(int action){
    return GetAsyncKeyState(action)&0x8000;//返回该活动是否是指定活动
};
void Get_Click_Pos_Of_Screen(POINT *pos,bool flag=0){//flag表示检测点击状态（0）还是非点击状态  （1） 
	while(1){//每隔0.1秒检查是否有点击事件
	    if(Clicked(VK_LBUTTON)^flag){
	        GetCursorPos(pos);//获取鼠标的绝对坐标
	        break;
	    }
        if(kbhit())getch();
	    Sleep(50);
	}
};
pair<int,int> Get_Click_Pos_Of_Client(int fla=0){//fla表示检测点击状态（0）还是非点击状态 （1） 
    HWND forewin=GetForegroundWindow();//获取顶层窗口句柄
    HANDLE outwin=GetStdHandle(STD_OUTPUT_HANDLE);//获取标准输出句柄 
    CONSOLE_FONT_INFO coninfor;//新建对象以存储控制台信息 
    GetCurrentConsoleFont(outwin,FALSE,&coninfor);//获取控制台信息 
    POINT pos;//新建坐标对象 
    Get_Click_Pos_Of_Screen(&pos,fla);//获取绝对坐标
    ScreenToClient(forewin,&pos);//将绝对坐标转为相对坐标 
    return make_pair(pos.y/(coninfor.dwFontSize.Y),pos.x/(coninfor.dwFontSize.X));
};
pair<int,int> Get_One_Click_Pos(){
    pair<int,int> clipos(0,0);
    pair<int,int> unclipos(0,1);
    POINT pos;//新建坐标对象 
    // char ch;
    while(clipos!=unclipos){
    	clipos=Get_Click_Pos_Of_Client(0);//获取点击状态的位置 
    	unclipos=Get_Click_Pos_Of_Client(1);//获取非点击状态的位置 
//    	if(clipos.first==unclipos.first&&clipos.second==unclipos.second){
	}
    // while(ch=getch());
    return clipos;
};
int Get_Signal(pair<int,int> &coor){
    int ret=0,tab=0;
    pair<int,int> clipos(-1,-1);
    pair<int,int> unclipos(-2,-2);
    HWND forewin=GetForegroundWindow();//获取顶层窗口句柄
    HANDLE outwin=GetStdHandle(STD_OUTPUT_HANDLE);//获取标准输出句柄 
    CONSOLE_FONT_INFO coninfor;//新建对象以存储控制台信息 
    GetCurrentConsoleFont(outwin,FALSE,&coninfor);//获取控制台信息 
    POINT pos;//新建坐标对象 
    // char ch;
    while(clipos!=unclipos){
	    Sleep(100);
        GetCursorPos(&pos);//获取鼠标的绝对坐标
        ScreenToClient(forewin,&pos);
        if(Clicked(VK_LBUTTON)){
            clipos=make_pair(pos.y/(coninfor.dwFontSize.Y),pos.x/(coninfor.dwFontSize.X));
        }
        else{
            unclipos=make_pair(pos.y/(coninfor.dwFontSize.Y),pos.x/(coninfor.dwFontSize.X));
        }
        if(Clicked(VK_TAB)){
            tab=1;
        }
        else {
            if(tab==1)return 1;
        }
        if(Clicked(VK_RETURN)){
            ret=1;
        }
        else{
            if(ret==1)return 2;
        }
	}
    coor=clipos;
    return 0;//如果出现点击事件则返回0
}
/*
按钮闪烁函数
*/
void Reverse_Color(const Button &but){
    Set_Color(1);
    but.Print();
    Sleep(100);
    Set_Color(0);
    but.Print();
}
void Reverse_Color(const Button_Box &but){
    Set_Color(1);
    but.Print();
    Sleep(100);
    Set_Color(0);
    but.Print();
}
/*
打印当前按钮
*/
void Button::Print()const{
    curmove(y(),x());
    cout<<text<<flush;
}
void Button_Box::Print()const{
    curmove(y(),x());
    cout<<text<<flush;
}
void Text_Box::Print()const{
    curmove(y(),x());
    cout<<guidetext<<flush;
}
/*
打印当前表单
*/
void Form::Print()const{
    for(auto text:input){
        text.Print();
    }
    for(auto but:singlebut){
        if(but.clickable()){
                but.Print();
        }
    }
    if(commitbut!=nullptr)(*commitbut).Print();
}