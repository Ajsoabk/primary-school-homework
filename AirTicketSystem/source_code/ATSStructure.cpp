#include <iostream>
#include <algorithm>
#include <malloc.h>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include "ATS.h"
using std::string;
using std::transform;
namespace pg{
    Page welcome,login,signup,forget,home,infor,order,buy,authen,addfli,adminfli,adminord;

}
/*

*/
int Page::translate(string str)const {
    transform(str.begin(),str.end(),str.begin(),::tolower);
    if(options.count(str)){
        return options.at(str);
    }
    else return 0;//
}
System::System(){
    pg::welcome.Ini_Welcome();
    pg::welcome.funcPtr=Welcome_Page;

    pg::login.Ini_Log_In();
    pg::login.funcPtr=Log_In_Page;

    pg::signup.Ini_Sign_Up();
    pg::signup.funcPtr=Sign_Up_Page;

    pg::forget.Ini_Forget();
    pg::forget.funcPtr=Forget_Page;

    pg::home.Ini_Home();
    pg::home.funcPtr=Home_Page;

    pg::infor.Ini_Infor();
    pg::infor.funcPtr=Infor_Page;

    pg::order.Ini_Order();
    pg::order.funcPtr=Order_Page;
    
    pg::buy.Ini_Buy();
    pg::buy.funcPtr=Buy_Page;
    
    pg::authen.Ini_Authen();
    pg::authen.funcPtr=Authen_Page;

    pg::addfli.Ini_Addfli();
    pg::addfli.funcPtr=Addfli_Page;

    pg::adminfli.Ini_Adminfli();
    pg::adminfli.funcPtr=Adminfli_Page;

    pg::adminord.Ini_Adminord();
    pg::adminord.funcPtr=Adminord_Page;

    page=pg::welcome;
}
Page::Page(){
    options.insert(make_pair("fresh",-3));
    options.insert(make_pair("back",-2));
    options.insert(make_pair("exit",-1));
}

void Page::Ini_Welcome(){
    options.insert(make_pair("login",1));
    options.insert(make_pair("signup",2));

    optionsPtr.insert(make_pair(translate("login"),&(pg::login)));
    optionsPtr.insert(make_pair(translate("signup"),&(pg::signup)));
}
void Page::Ini_Log_In(){
    options.insert(make_pair("home",1));
    options.insert(make_pair("forget",2));

    
    optionsPtr.insert(make_pair(translate("home"),&(pg::home)));
    optionsPtr.insert(make_pair(translate("forget"),&(pg::forget)));
}
void Page::Ini_Sign_Up(){
    
}
void Page::Ini_Forget(){
    
}
void Page::Ini_Home(){
    options.insert(make_pair("infor",2));
    options.insert(make_pair("order",3));
    options.insert(make_pair("buy",4));
    options.insert(make_pair("adminfli",5));
    options.insert(make_pair("adminord",6));


    optionsPtr.insert(make_pair(translate("buy"),&(pg::buy)));
    optionsPtr.insert(make_pair(translate("infor"),&(pg::infor)));
    optionsPtr.insert(make_pair(translate("order"),&(pg::order)));
    optionsPtr.insert(make_pair(translate("adminfli"),&(pg::adminfli)));
    optionsPtr.insert(make_pair(translate("adminord"),&(pg::adminord)));
}
void Page::Ini_Addfli(){

}
void Page::Ini_Adminfli(){
    options.insert(make_pair("addfli",1));
    
    optionsPtr.insert(make_pair(translate("addfli"),&(pg::addfli)));
}
void Page::Ini_Infor(){
    options.insert(make_pair("authen",5));

    optionsPtr.insert(make_pair(translate("authen"),&(pg::authen)));

}
void Page::Ini_Adminord(){

}
void Page::Ini_Order(){
    
}
void Page::Ini_Buy(){
    
}
void Page::Ini_Authen(){

}