#include <iostream>
#include <malloc.h>
#include <windows.h>
#include <fstream>
#include <string>
#include <vector>
#include <regex>
#include <map>
#include "ATS.h"
using std::ifstream;
using std::ofstream;
using std::string;
using std::regex;
using std::to_string;
using std::regex_match;
using std::endl;
/*
定义固定的文件路径
*/
namespace path{
    //顾客相关
    const string id="../data/CustomerID.DAT";
    const string name="../data/Customername.DAT";
    const string sex="../data/CustomerSex.DAT";
    const string tele="../data/CustomerTele.DAT";
    const string email="../data/CustomerEmail.DAT";
    const string status="../data/CustomerStatus.DAT";
    const string belong="../data/AdminCompany.DAT";
    //航班相关
    const string num="../data/FlightNumber.DAT";
    const string com="../data/FlightCompany.DAT";
    const string city="../data/FlightCity.DAT";
    const string model="../data/FlightModel.DAT";
    const string pilot="../data/FlightPilot.DAT";
    const string time="../data/FlightTime.DAT";
    const string remain="../data/FlightRemained.DAT";
    const string prices="../data/FlightPrices.DAT";
    const string rate="../data/FlightRate.DAT";
    //订单相关
    const string ordid="../data/OrderID.DAT";
    const string ordnum="../data/Ordnum.DAT";
    const string ordtim="../data/Ordtim.DAT";
    const string payed="../data/OrderPayed.DAT";
    const string ordname="../data/OrderName.DAT";
    const string ordcit="../data/OrderCity.DAT";
    const string ordseat="../data/OrderSeat.DAT";
    const string ordse="../data/OrderSettime.DAT";
    const string ordpri="../data/OrderPrice.DAT";
    const string ordcom="../data/OrderCompany.DAT";
};
/*
数据追加模板
将该数据插入到该文件末尾
*/
template<class T>
void Append_In(const string &path,const T &str){
    ofstream fout;
    fout.open(path,ios::app);
    fout<<str<<"\n";
    fout.close();
}
/*
用户数据追加函数
*/
void Customer::Write()
{
    Append_In(path::id,id+" "+pwd);
    Append_In(path::name,name);
    Append_In(path::sex,sex);
    Append_In(path::tele,tele);
    Append_In(path::email,email);
    Append_In(path::status,status);
    Append_In(path::belong,company);
}
/*
航班数据追加函数
*/
void Flight::Write(){
    Append_In(path::num,num);
    Append_In(path::com,com);
    Append_In(path::city,cit[0]+" "+cit[1]);
    Append_In(path::model,mod);
    Append_In(path::pilot,pil[0]+" "+pil[1]);
    Append_In(path::time,stime+" "+etime);
    Append_In(path::remain,to_string(rem[0])+" "+to_string(rem[1])+" "+to_string(rem[2]));
    Append_In(path::rate,rat);
    Append_In(path::prices,to_string(pri[0])+" "+to_string(pri[1])+" "+to_string(pri[2]));
}

/*
订单数据追加函数
*/
void Order::Write(){
    Append_In(path::ordid,id);
    Append_In(path::ordnum,num);
    Append_In(path::ordtim,tim);
    Append_In(path::ordname,name);
    Append_In(path::ordseat,seat);
    Append_In(path::ordcit,cit[0]+" "+cit[1]);
    Append_In(path::ordse,stime+" "+etime);
    Append_In(path::ordpri,pri);
    Append_In(path::payed,payed);
    Append_In(path::ordcom,com);
}
/*
获取密码函数
根据传入的ID返回对应密码
*/
string Check_By_ID(const string &ID_now){
    ifstream fid(path::id);
    string pwd="";
    for(string str="\n";(fid>>str>>pwd)&&str!=ID_now;);
    fid.close();
    return pwd;
}
/*
判断该样本在该文件内是否存在
*/
bool Is_Unique_In(const string& path,const string& example){
    ifstream fin(path);
    string str;
    while(fin>>str){
        if(str==example)return false;
    }
    fin.close();
    return true;
}

bool Unique_ID(const string &example){
    return Is_Unique_In(path::id,example);
}
bool Unique_Company_Name(const string &example){
    return Is_Unique_In(path::belong,example);
}
bool Unique_Number(const string &example){
    return Is_Unique_In(path::num,example);
}
bool Unique_Tele(const string &example){
    return Is_Unique_In(path::tele,example);
}
bool Unique_Email(const string &example){
    return Is_Unique_In(path::email,example);
}
bool Order_Conflict(const Order& example,const Order& ord){
    return !((example.etime<ord.stime)||(example.stime>ord.etime));
}

bool Legal_Order(const Order &ord,string myid){
    vector<Order> allord;
    Check_All_Order(allord);
    vector<Order> myord=allord;
    Filter_Based_On_ID(myord,myid);
    if(myord.size()==5)return false;
    for(auto noword:myord){
        if(Order_Conflict(noword,ord)){
            return false;
        }
    }
    return true;
}
//正则匹配
bool Legal_ID(const string &str){
    regex rule("^\\w{6,10}$");
    return regex_match(str,rule);
}
bool Legal_Number(const string &str){
    regex rule("^[a-zA-Z]{2}\\d{4}");
    return regex_match(str,rule);
}
bool Legal_Digit(const string &str){
    regex rule("^\\d+$");
    return regex_match(str,rule);
}
bool Legal_Name(const string &str){
    regex rule("^[a-z]+$",regex::icase);
    if (regex_match(str,rule)) return true;
    int nlen = str.length();
    for(int i=0; i<nlen; i++)
    {
        if(str[i]>=0&&str[i]<=127){
            return false; //不是全角字符
        }
    }

    return true;
}

bool Legal_Pwd(const string &str)
{
    regex rule("^\\w{6,}$");
    return regex_match(str,rule);
}

bool Legal_Tele(const string &str)
{
    regex rule("^\\d{11}$");
    return regex_match(str,rule);
}

bool Legal_Email(const string &str)
{
    regex emailReg("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");
    return regex_match(str,emailReg);
}
bool Legal_Time(const string &str)
{
    regex rule("^[0-2][0-9]:[0-5][0-9]$");
    if(regex_match(str,rule)){
        if(str[0]=='2'&&str[1]>='4')return false;
        else return true;
    }
    return false;
}
/*
根据行数获取data
*/
template<class T>
void Get_Data_By_Line(const string &path,int num,T& data1){
    ifstream fin(path);
    for(int i=1;i<=num;++i)fin>>data1;
    fin.close();
}
template<class T>
void Get_Data_By_Line(const string &path,int num,T& data1,T& data2){
    ifstream fin(path);
    for(int i=1;i<=num;++i)fin>>data1>>data2;
    fin.close();
}
template<class T>
void Get_Data_By_Line(const string &path,int num,T& data1,T& data2,T& data3){
    ifstream fin(path);
    for(int i=1;i<=num;++i)fin>>data1>>data2>>data3;
    fin.close();
}
/*
根据data找到行数
*/
int Get_Line_By_Data(const string &path,const string &str){
    ifstream fin(path);
    int ret=1;
    string nowdata;
    while(fin>>nowdata){
        if(nowdata==str)return ret;
        ret++;
    }
    fin.close();
    return 0;
}
/*
把顾客信息补充完整
*/
int Customer::Complete(){
    int cnt=1;
    ifstream idin(path::id);
    for(string nowid;(idin>>nowid>>pwd)&&nowid!=id;++cnt);
    idin.close();
    Get_Data_By_Line(path::name,cnt,name);
    Get_Data_By_Line(path::tele,cnt,tele);
    Get_Data_By_Line(path::email,cnt,email);
    Get_Data_By_Line(path::sex,cnt,sex);
    Get_Data_By_Line(path::status,cnt,status);
    Get_Data_By_Line(path::belong,cnt,company);
    return 0;
}
/*
把航班信息补充完整
*/
void Flight::Complete(){
    int cnt=Get_Line_By_Data(path::num,num);
    Get_Data_By_Line(path::com,cnt,com);
    Get_Data_By_Line(path::model,cnt,mod);
    Get_Data_By_Line(path::city,cnt,cit[0],cit[1]);
    Get_Data_By_Line(path::pilot,cnt,pil[0],pil[1]);
    Get_Data_By_Line(path::time,cnt,stime,etime);
    Get_Data_By_Line(path::remain,cnt,rem[0],rem[1],rem[2]);
    Get_Data_By_Line(path::prices,cnt,pri[0],pri[1],pri[2]);
    Get_Data_By_Line(path::rate,cnt,rat);
}

/*
根据行数及数据修改文件
*/
template<class T>
void Modify_By_Line(const string& path,int line,const T &data){
    ostringstream strout;
    ifstream fin(path);
    string str;
    for(int i=1,fla=0;(fla||i<=line)&&(getline(fin,str));i++){
        if(i==line){
            strout<<data<<"\n";
            fla=1;
        }
        else strout<<str<<"\n";
    }
    fin.close();
    ofstream fout(path);
    fout<<strout.str();
    fout.close();
}
/*
修改顾客信息
*/
void Customer::Modify(string mod){
    ifstream fin(path::id);
    string str,str2;
    int cnt=1;
    for(;(fin>>str>>str2)&&(str!=id);++cnt);
    fin.close();
    if(mod=="all"||mod=="name")Modify_By_Line(path::name,cnt,name);
    if(mod=="all"||mod=="sex")Modify_By_Line(path::sex,cnt,sex);
    if(mod=="all"||mod=="tele")Modify_By_Line(path::tele,cnt,tele);
    if(mod=="all"||mod=="email")Modify_By_Line(path::email,cnt,email);
    if(mod=="all"||mod=="status")Modify_By_Line(path::status,cnt,status);
    if(mod=="all"||mod=="company")Modify_By_Line(path::belong,cnt,company);
}
/*
修改航班信息
*/
void Flight::Modify(string mod){
    int cnt=Get_Line_By_Data(path::num,num);
    if(mod=="all"||mod=="com")Modify_By_Line(path::com,cnt,com);
    if(mod=="all"||mod=="city")Modify_By_Line(path::city,cnt,cit[0]+" "+cit[1]);
    if(mod=="all"||mod=="model")Modify_By_Line(path::model,cnt,mod);
    if(mod=="all"||mod=="pilot")Modify_By_Line(path::pilot,cnt,pil[0]+" "+pil[1]);
    if(mod=="all"||mod=="time")Modify_By_Line(path::time,cnt,stime+" "+etime);
    if(mod=="all"||mod=="remain")Modify_By_Line(path::remain,cnt,to_string(rem[0])+" "+to_string(rem[1])+" "+to_string(rem[2]));
    if(mod=="all"||mod=="price")Modify_By_Line(path::prices,cnt,to_string(pri[0])+" "+to_string(pri[1])+" "+to_string(pri[2]));
    if(mod=="all"||mod=="rat")Modify_By_Line(path::rate,cnt,rat);
}
void Flight::bought(const int &num){
    rem[num]--;
    Modify("remain");
}
/*
获取所有航班
*/
void Check_All_Flight(vector<Flight> &ret){
    ret.clear();
    ifstream numin(path::num);
    ifstream comin(path::com);
    ifstream citin(path::city);
    ifstream modin(path::model);
    ifstream pilin(path::pilot);
    ifstream timin(path::time);
    ifstream remin(path::remain);
    ifstream priin(path::prices);
    ifstream ratin(path::rate);
    string str;
    //先读入航班号
    while(numin>>str){
        ret.push_back(Flight(str));
    }
    numin.close();
    
    for(auto &fli:ret){
        comin>>fli.com;
        citin>>fli.cit[0]>>fli.cit[1];
        modin>>fli.mod;
        pilin>>fli.pil[0]>>fli.pil[1];
        timin>>fli.stime>>fli.etime;
        remin>>fli.rem[0]>>fli.rem[1]>>fli.rem[2];
        priin>>fli.pri[0]>>fli.pri[1]>>fli.pri[2];
        ratin>>fli.rat;
    }
    comin.close();
    citin.close();
    modin.close();
    pilin.close();
    timin.close();
    remin.close();
    priin.close();
    ratin.close();
}
/*
获取所有订单
*/
void Check_All_Order(vector<Order> &ret){
    ret.clear();
    ifstream idin(path::ordid);
    ifstream numin(path::ordnum);
    ifstream namein(path::ordname);
    ifstream timin(path::ordtim);
    ifstream seatin(path::ordseat);
    ifstream payedin(path::payed);
    ifstream citin(path::ordcit);
    ifstream priin(path::ordpri);
    ifstream settimein(path::ordse);
    ifstream comin(path::ordcom);
    string str;
    while(idin>>str){
        Order ord;
        ord.id=str;
        numin>>ord.num;
        namein>>ord.name;
        timin>>ord.tim;
        seatin>>ord.seat;
        payedin>>ord.payed;
        citin>>ord.cit[0]>>ord.cit[1];
        priin>>ord.pri;
        settimein>>ord.stime>>ord.etime;
        comin>>ord.com;
        ret.push_back(ord);
    }
    numin.close();
    idin.close();
    numin.close();
    timin.close();
    seatin.close();
    payedin.close();
    citin.close();
    priin.close();
    settimein.close();
    comin.close();
}
/*
将容器（vec）内的元素满足条件（condition）的都筛去
*/
template<class Container,class Example>
void Filter_Based_On(vector<Container> &vec,Example str,bool (*condition)(const Container&,const Example&)){
    int i=0;
    while(i<vec.size()){
        if((*condition)(vec[i],str)==1){
            typename vector<Container>::iterator it=vec.begin()+i;
            vec.erase(it);
        }
        else i++;
    }
}

bool Filter_ID_Condition(const Order &ord,const string& str){
    return ord.id!=str;
}
/*
把id不等于cusid的订单筛去
*/
void Filter_Based_On_ID(vector<Order>& vec,string cusid){
    Filter_Based_On(vec,cusid,Filter_ID_Condition);
}

bool Filter_Company_Condition(const Order &ord,const string& str){
    return ord.com!=str;
}
void Filter_Based_On_Company(vector<Order>& vec,string comname){
    Filter_Based_On(vec,comname,Filter_Company_Condition);
}

//航班信息过滤
//mode为0时表示只保留该company的航班，为1时表示把该company的航班滤去
bool Filter_Company_Condition1(const Flight &fli,const string &str){
    return fli.com!=str;
}
bool Filter_Company_Condition2(const Flight &fli,const string &str){
    return fli.com==str;
}
void Filter_Based_On_Company(vector<Flight>& vec,string str,bool mode){

    if(mode==0)Filter_Based_On(vec,str,Filter_Company_Condition1);
    else Filter_Based_On(vec,str,Filter_Company_Condition2);
}

bool Filter_City_Condition1(const Flight &fli,const string &str){
    return fli.cit[0]!=str;
}
bool Filter_City_Condition2(const Flight &fli,const string &str){
// cout<<endl<<fli.cit[1]<<" "<<str<<(fli.cit[1]!=str);
    return fli.cit[1]!=str;
}
void Filter_Based_On_City(vector<Flight>& vec,string str1,string str2){
// for(int i=0;i<vec.size();++i){
// cout<<endl<<vec[i].cit[0]<<"!"<<vec[i].cit[1]<<"?"<<str1;Sleep(2000);
// }
// cout<<"!"<<vec.size();Sleep(2000);
    Filter_Based_On(vec,str1,Filter_City_Condition1);
// cout<<"?"<<vec.size();Sleep(2000);
    Filter_Based_On(vec,str2,Filter_City_Condition2);
}

bool Filter_Time_Condition1(const Flight &fli,const string &str){
    return fli.stime<str;
}
bool Filter_Time_Condition2(const Flight &fli,const string &str){
    return fli.stime>str;
}
//mode为0时表示只保留起飞时间在mtime之后的航班,为1则相反
void Filter_Based_On_Stime(vector<Flight>& vec,string str,bool mode){
    if(mode==0)Filter_Based_On(vec,str,Filter_Time_Condition1);
    else Filter_Based_On(vec,str,Filter_Time_Condition2);
}
//mode为0时表示只保留到达时间在mtime之前的航班,为1则相反，总之0都是正常需要，1都是特殊的相反需要
bool Filter_Time_Condition3(const Flight &fli,const string &str){
    return fli.etime>str;
}
bool Filter_Time_Condition4(const Flight &fli,const string &str){
    return fli.etime<str;
}
void Filter_Based_On_Etime(vector<Flight>& vec,string str,bool mode){
    if(mode==0)Filter_Based_On(vec,str,Filter_Time_Condition3);
    else Filter_Based_On(vec,str,Filter_Time_Condition4);
}
void Filter_Based_On_Time(vector<Flight>& vec,string stime,string etime){
    Filter_Based_On_Stime(vec,stime);
    Filter_Based_On_Etime(vec,etime);
}
//航班信息排序
/*
command有
"stime":根据出发时间顺序排列
"etime":根据到达时间顺序排列
"prices":根据最低价格（prices[0]）从低到高
"rate":根据准点率排序
*/
bool cmp_stime(const Flight &a,const Flight &b){
        return a.stime<b.stime;
}
bool cmp_etime(const Flight &a,const Flight &b){
        return a.etime<b.etime;
}
bool cmp_prices(const Flight &a,const Flight &b){
    return a.pri[0]<b.pri[0];
}
bool cmp_rate(const Flight &a,const Flight &b){
    return a.rat>b.rat;
}
bool cmp_ordtime(const Order &a,const Order &b){
    return (a.stime==b.stime)?a.etime<b.etime:a.stime<b.stime;
}
vector<Order> Order_Sort(vector<Order> ord,string command){
    if(command=="time"){
        sort(ord.begin(),ord.end(),cmp_ordtime);
    }
    return ord;
}
vector<Flight> Flight_Sort(vector<Flight> f,string command){
    if(command=="stime"){
        sort(f.begin(),f.end(),cmp_stime);
    }
    else if(command=="etime"){
        sort(f.begin(),f.end(),cmp_etime);
    }
    else if(command=="prices"){
        sort(f.begin(),f.end(),cmp_prices);
    }
    else if(command=="rate"){
        sort(f.begin(),f.end(),cmp_rate);
    }
    return f;
}
/*
返回当前时间
*/
string Get_Time()
{
    time_t rawtime; 
    struct tm *ptminfo; 
    string a;
    char str[50];
    time(&rawtime); 
    ptminfo = localtime(&rawtime); 
    sprintf(str,"%02d-%02d-%02d %02d:%02d:%02d", 
            ptminfo->tm_year + 1900, ptminfo->tm_mon + 1, ptminfo->tm_mday, 
            ptminfo->tm_hour, ptminfo->tm_min, ptminfo->tm_sec); 
    a=str;    
    return  a; 
}