#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stack>
#include <map>
#include "ATS.h"
using namespace std;
int main(){
    full_screen();
    Unable_Quick_Edit_Mode();
    System oursystem;
    // Welcome();
    string input;
    oursystem.sta.push(oursystem.page);
    int opt=(*(oursystem.page.funcPtr))(oursystem.page);
    while(opt!=-1){
        if(opt==-2){
            oursystem.sta.pop();
            oursystem.page=oursystem.sta.top();
        }
        else {
            oursystem.page=*(oursystem.page.optionsPtr.at(opt));
            oursystem.sta.push(oursystem.page);
        }
        opt=(*(oursystem.page.funcPtr))(oursystem.page);
        /*
        opt为-1 :关闭系统
        opt为1  :返回上一级
        opt>1   :跳转到其他页面
        */
    }
    return 0;
}