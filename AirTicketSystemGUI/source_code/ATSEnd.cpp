#include <iostream>
#include <malloc.h>
#include <fstream>
#include <string>
#include <vector>
#include <regex>
#include <map>
#include "ATS.h"
using std::ifstream;
using std::ofstream;
using std::string;
using std::regex;
using std::regex_match;
using std::endl;
namespace path{
    const string id="../data/CustomerID.DAT";
    const string name="../data/Customername.DAT";
    const string pwd="../data/CustomerPwd.DAT";
    const string sex="../data/CustomerSex.DAT";
    const string tele="../data/CustomerTele.DAT";
    const string email="../data/CustomerEmail.DAT";
    const string num="../data/FlightNumber.DAT";
    const string com="../data/FlightCompany.DAT";
};
int Customer::Write()
{
    ofstream fout;
    fout.open(path::id, ios::app);
    fout<<id<<"\n";
    fout.close();
    fout.open(path::name, ios::app);
    fout<<name<<"\n";
    fout.close();
    fout.open(path::pwd, ios::app);
    fout<<pwd<<"\n";
    fout.close();
    fout.open(path::sex, ios::app);
    fout<<sex<<"\n";
    fout.close();
    fout.open(path::tele, ios::app);
    fout<<tele<<"\n";
    fout.close();
    fout.open(path::email, ios::app);
    fout<<email<<"\n";
    fout.close();
    return 0;
}
int Flight::Write(){
    ofstream fout;
    return 0;
}
string Check_By_ID(string ID_now){
    ifstream fid(path::id);
    ifstream fpwd(path::pwd);
    int cnt=0;
    for(string str="\n";str!=ID_now;cnt++){
        fid>>str;
    }
    fid.close();
    string pwd;
    for(int i=1;i<cnt;++i)fpwd>>pwd;
    fpwd>>pwd;
    fpwd.close();
    return pwd;
}
bool Unique_ID(string example){
    ifstream fin(path::id);
    string str="\n";
    while((fin>>str)&&str!=example);
    fin.close();
    return str!=example;
}
bool Unique_Number(string example){
    ifstream fin(path::num);
    string str="\n";
    while((fin>>str)&&str!=example);
    fin.close();
    return str!=example;
}
bool Unique_Tele(string example){
    ifstream fin(path::tele);
    string str="\n";
    while((fin>>str)&&str!=example);
    fin.close();
    return str!=example;
}
bool Unique_Email(string example){
    ifstream fin(path::email);
    string str="\n";
    while((fin>>str)&&str!=example);
    fin.close();
    return str!=example;
}

bool Legal_ID(string str){
    regex rule("^\\w{6,10}$");
    return regex_match(str,rule);
}

bool Legal_Name(string str){
    regex rule("^[a-z]$",regex::icase);
    if (regex_match(str,rule)) return true;
    int nlen = str.length();
    for(int i=0; i<nlen; i++)
    {
        if(str[i]>=0&&str[i]<=127){
            return false; //不是全角字符
        }
    }

    return true;
}

bool Legal_Pwd(string str)
{
    regex rule("^\\w{6,}$");
    return regex_match(str,rule);
}

bool Legal_Tele(string str)
{
    regex rule("^\\d{11}$");
    return regex_match(str,rule);
}

bool Legal_Email(string str)
{
    regex emailReg("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");
    return regex_match(str,emailReg);
}
int Customer::Complete(){
    ifstream idin(path::id);
    ifstream namein(path::name);
    ifstream telein(path::tele);
    ifstream emailin(path::email);
    int cnt=0;
    for(string nowid;(idin>>nowid)&&nowid!=id;++cnt);
    for(int i=0;i<=cnt;++i)namein>>name;
    for(int i=0;i<=cnt;++i)telein>>tele;
    for(int i=0;i<=cnt;++i)emailin>>email;
    idin.close();
    namein.close();
    telein.close();
    emailin.close();
    return 0;
}

void Customer::Modify_Name(int pos){
    ostringstream strout;
    ifstream fin;
    ofstream fout;
    string str;
    fin.open(path::name);
    for(int i=1,fla=0;(fla||i<pos)&&(fin>>str);i++){
        if(i==pos)strout<<name<<endl;
        else strout<<str<<endl;
    }
    fin.close();
    fout.open(path::name);
    fout<<strout.str();
    fout.close();
}
void Customer::Modify_Pwd(int pos){
    ostringstream strout;
    ifstream fin;
    ofstream fout;
    string str;
    fin.open(path::pwd);
    for(int i=1,fla=0;(fla||i<pos)&&(fin>>str);i++){
        if(i==pos)strout<<pwd<<endl;
        else strout<<str<<endl;
    }
    fin.close();
    fout.open(path::pwd);
    fout<<strout.str();
    fout.close();
}
void Customer::Modify_Sex(int pos){
    ostringstream strout;
    ifstream fin;
    ofstream fout;
    string str;
    fin.open(path::sex);
    for(int i=1,fla=0;(fla||i<pos)&&(fin>>str);i++){
        if(i==pos)strout<<sex<<endl;
        else strout<<str<<endl;
    }
    fin.close();
    fout.open(path::sex);
    fout<<strout.str();
    fout.close();
}
void Customer::Modify_Tele(int pos){
    ostringstream strout;
    ifstream fin;
    ofstream fout;
    string str;
    fin.open(path::tele);
    for(int i=1,fla=0;(fla||i<pos)&&(fin>>str);i++){
        if(i==pos)strout<<tele<<endl;
        else strout<<str<<endl;
    }
    fin.close();
    fout.open(path::tele);
    fout<<strout.str();
    fout.close();
}
void Customer::Modify_Email(int pos){
    ostringstream strout;
    ifstream fin;
    ofstream fout;
    string str;
    fin.open(path::email);
    for(int i=1,fla=0;(fla||i<pos)&&(fin>>str);i++){
        if(i==pos)strout<<email<<endl;
        else strout<<str<<endl;
    }
    fin.close();
    fout.open(path::email);
    fout<<strout.str();
    fout.close();
}
void Customer::Modify(){
    ifstream fin;
    string str;
    int cnt=1;
    for(;(fin>>str)&&(str!=id);++cnt);
    Modify_Pwd(cnt);
    Modify_Name(cnt);
    Modify_Sex(cnt);
    Modify_Tele(cnt);
    Modify_Email(cnt);
}
void Span::print(){
    curmove(t,l);
    for(int len=r-l+1;len;len--)putchar(hor_bor);
    
}

/*
void change_User_name(Customer u,int pos){
    int n=0;
    string str;
    freopen("User_name.txt","r",stdin);
    freopen("temp.txt","w",stdout);

    while(n!=pos){
        cin>>str;
        cout<<str<<endl;
        str.clear();
        n++;
    }
    cin>>str;cout<<u.name<<endl;str.clear();
    cin>>str;
    while(str.length()!=0){
        cout<<str<<endl;
        str.clear();
        cin>>str;
    }
    fclose(stdin);cin.clear();
    fclose(stdout);cout.clear();
    system("del User_name.txt");
    system("rename temp.txt User_name.txt");
}

void change_User_pwd(Customer u,int pos){
    int n=0;
    string str;
    freopen("User_pwd.txt","r",stdin);
    freopen("temp.txt","w",stdout);

    while(n!=pos){
        cin>>str;
        cout<<str<<endl;
        n++;
        str.clear();
    }
    cin>>str;cout<<u.pwd<<endl;str.clear();
    cin>>str;
    while(str.length()!=0){
        cout<<str<<endl;
        str.clear();
        cin>>str;
    }
    fclose(stdin);cin.clear();
    fclose(stdout);cout.clear();
    system("del User_pwd.txt");
    system("rename temp.txt User_pwd.txt");
}



void change_User_sex(Customer u,int pos){
    int n=0;
    string str;
    freopen("User_sex.txt","r",stdin);
    freopen("temp.txt","w",stdout);

    while(n!=pos){
        cin>>str;
        cout<<str<<endl;
        n++;
        str.clear();
    }
    cin>>str;cout<<u.sex<<endl;str.clear();
    cin>>str;
    while(str.length()!=0){
        cout<<str<<endl;
        str.clear();
        cin>>str;
    }
    fclose(stdin);cin.clear();
    fclose(stdout);cout.clear();
    system("del User_sex.txt");
    system("rename temp.txt User_sex.txt");
}



void change_User_tele(Customer u,int pos){
    int n=0;
    string str;
    freopen("User_tele.txt","r",stdin);
    freopen("temp.txt","w",stdout);

    while(n!=pos){
        cin>>str;
        cout<<str<<endl;
        n++;
        str.clear();
    }
    cin>>str;cout<<u.tele<<endl;str.clear();
    cin>>str;
    while(str.length()!=0){
        cout<<str<<endl;
        str.clear();
        cin>>str;
    }
    fclose(stdin);cin.clear();
    fclose(stdout);cout.clear();
    system("del User_tele.txt");
    system("rename temp.txt User_tele.txt");
}



void change_User_email(Customer u,int pos){
    int n=0;
    string str;
    freopen("User_email.txt","r",stdin);
    freopen("temp.txt","w",stdout);

    while(n!=pos){
        cin>>str;
        cout<<str<<endl;
        n++;
        str.clear();
    }
    cin>>str;cout<<u.email<<endl;str.clear();
    cin>>str;
    while(str.length()!=0){
        cout<<str<<endl;
        str.clear();
        cin>>str;
    }
    fclose(stdin);cin.clear();
    fclose(stdout);cout.clear();
    system("del User_email.txt");
    system("rename temp.txt User_email.txt");
}


void change_User(Customer u){
    freopen("User_ID.txt","r",stdin);
    string str;
    int pos=0;
    cin>>str;
    while(str!=u.id){
        pos++;
        str.clear();
        cin>>str;
    }
    fclose(stdin);
    cin.clear();


    change_User_name(u,pos);
    change_User_pwd(u,pos);
    change_User_sex(u,pos);
    change_User_tele(u,pos);
    change_User_email(u,pos);

    freopen("CON","r",stdin);
}
*/
