#include <iostream>
#include <windows.h>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <string>
#include <vector>
#include <conio.h>
#include "ATS.h"
using namespace std;
string System::User_ID;

void curmove(short y,short x=0)//定位到y行x列
{
    HANDLE hOut; //新建句柄 hOut
    hOut=GetStdHandle(STD_OUTPUT_HANDLE);  //实例化句柄 hOut
    COORD pos={x,y};
    SetConsoleCursorPosition(hOut,pos);
};

bool Clicked(int action){
    return GetAsyncKeyState(action)&0x8000;//返回该活动是否是指定活动
}
void Unable_Quick_Edit_Mode(){
    HANDLE stdwin=GetStdHandle(STD_INPUT_HANDLE);//获取输出窗口句柄
    DWORD mode;
    GetConsoleMode(stdwin,&mode);//获取该窗口的模式信息
    mode &= ~ENABLE_QUICK_EDIT_MODE;//在模式信息中取消掉快速编辑模式
    SetConsoleMode(stdwin,mode);
}
void Get_Click_Pos_Of_Screen(POINT *pos,bool flag=0){//flag表示检测点击状态（0）还是非点击状态  （1） 
	while(1){//每隔0.1秒检查是否有点击事件
	    if(Clicked(VK_LBUTTON)^flag){
	        GetCursorPos(pos);//获取鼠标的绝对坐标
	        break;
	    }
	    Sleep(50);
	}
}
pair<int,int> Get_Click_Pos_Of_Client(int fla=0){//fla表示检测点击状态（0）还是非点击状态 （1） 
    HWND forewin=GetForegroundWindow();//获取顶层窗口句柄
    HANDLE outwin=GetStdHandle(STD_OUTPUT_HANDLE);//获取标准输出句柄 
    CONSOLE_FONT_INFO coninfor;//新建对象以存储控制台信息 
    GetCurrentConsoleFont(outwin,FALSE,&coninfor);//获取控制台信息 
    POINT pos;//新建坐标对象 
    Get_Click_Pos_Of_Screen(&pos,fla);//获取绝对坐标
    ScreenToClient(forewin,&pos);//将绝对坐标转为相对坐标 
    return make_pair(pos.y/(coninfor.dwFontSize.Y),pos.x/(coninfor.dwFontSize.X));
}
pair<int,int> Get_One_Click_Pos(){
    pair<int,int> clipos;
    pair<int,int> unclipos;
    while(1){
    	clipos=Get_Click_Pos_Of_Client();//获取点击状态的位置 
    	unclipos=Get_Click_Pos_Of_Client(1);//获取非点击状态的位置 
//    	if(clipos.first==unclipos.first&&clipos.second==unclipos.second){
		if(clipos==unclipos){//如果是在同一个坐标处按下并弹起 
	    	return clipos;
	    }
        else Sleep(100);
	}
}
string& trim(std::string &s){
    if(!s.empty()){
        s.erase(0,s.find_first_not_of(" "));
        s.erase(s.find_last_not_of(" ")+1);
    }
    return s;
}
void full_screen(){
    HWND froWindow=GetForegroundWindow();
    int cx=GetSystemMetrics(SM_CXSCREEN);
    int cy=GetSystemMetrics(SM_CYSCREEN);

    LONG froWindowInfor=GetWindowLong(froWindow,GWL_STYLE);
    SetWindowLong(froWindow,GWL_STYLE,(froWindowInfor|WS_POPUP|WS_MAXIMIZE)& ~WS_CAPTION& ~WS_THICKFRAME& ~WS_BORDER);
    SetWindowPos(froWindow,HWND_TOP,0,0,cx,cy,0);
}
int Get_Center_Position(string str){
	int One_wid=GetSystemMetrics(SM_CXCURSOR);
	int wid=GetSystemMetrics( SM_CXFULLSCREEN);
	wid=wid*4/One_wid;
    return (wid-str.length())/2;
}
string Print_In_Center(string str){
    
	int pos=Get_Center_Position(str);
    ostringstream strout;
	for(int i=0;i<pos;++i){
        strout<<" ";
	}
    strout<<str;
    return strout.str();
}
void Move_TO_Upper_Line(){
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD coordScreen = {0, 0}; //光标位置
    CONSOLE_SCREEN_BUFFER_INFO csbi;
 
    if (GetConsoleScreenBufferInfo(hConsole, &csbi))
    {
        printf("光标坐标:(%d,%d)\n",  csbi.dwCursorPosition.X, csbi.dwCursorPosition.Y);
    }
}
void Print_List(vector<pair<string,string> > lis,int pos=0){
    int len=lis.size();
    if(pos==0){
        //找到最长的first元素，把pos设为此长度+1
        for(int i=0;i<len;i++)
        {
            pos=max(pos,(int)lis[i].first.length());
        }
        pos+=1;
    }
    for(int i=0;i<len;i++){
        cout<<std::right<<setw(pos)<<lis[i].first<<":"<<std::left<<lis[i].second<<endl;
    }
    
}
void printtext(const Span& sp,string text,int offsety,int offsetx){
    int y=sp.coor.first+offsety;
    int x=sp.coor.second+offsetx;
    curmove(y,x);
    cout<<text;
}
void setcurpos(Span &sp,int offsety,int offsetx){
    sp.cur_x=sp.coor.second+offsetx;
    sp.cur_y=sp.coor.first+offsety;
}
int System::Welcome_Page(const Page& curpage){
    system("cls");
    Span mainspan(1,1,50,200,' ','_');
    Span titlespan(0,0,3,200,' ','—',1,mainspan);
    Span freshspan(0,0,3,2,10,' ','—',1,titlespan);
    freshspan.type=3;
    Span loginform(5,40,50,100,' ',' ',1,mainspan);
    loginform.type=4;
    Span idinput(3,1,3,30,' ',' ',1,loginform);
    idinput.type=1;
    Span pwdinput(5,1,3,30,' ',' ',1,loginform)
    pwdinput.type=1;
    Span forgetpwd(7,1,3,30,' ',' ',1,loginform);
    forgetpwd.type=3;
    Span noid(9,1,3,30,' ',' ',1,loginform);
    noid.type=3;
    Span errtext(11,1,3,30,' ',' ',1,loginform);
    errtext.type=0;
    Span commitbot(13,1,3,30,'?',' ',1,loginform);
    commitbot.type=2;

    printtext(freshspan,"?刷新页面",0,0);
    printtext(titlespan,"航空管理系统",0,90);
    printtext(idinput,"用户名/电话/邮箱/：",0,0);
    printtext(pwdinput,"密码:",0,0);
    printtext(forgetpwd,"忘记密码？ 点此发送邮件",0,0);
    printtext(noid,"没有账号？ 点此注册账号",0,0);
    printtext(commitbot,"提交",0,0);

    setcurpos(idinput,0,18);
    setcurpos(pwdinput,0,18);
    pair<int,int> pos;
    while(1){
        pos=Get_One_Click_Pos();
        Span const &nowspan=*(curpage.spanptr[pos.first][pos.second]);
        if(nowspan!=NULL){
            if(nowspan.type==2){
                
            }
            else if(nowspan.type==1){
                curmove()
            }
    
    curmove(20);
    cout<<Print_In_Center("欢迎进入购票系统\n");
    cout<<Print_In_Center("请输入任意字符进行登录");
    getch();
    return curpage.translate("log");
};
int System::Log_Page(const Page& curpage){
    system("cls");
    curmove(20);
    cout<<Print_In_Center("已有账号？输入“1”登陆\n");
    cout<<Print_In_Center("尚未注册？输入“2”注册\n");
    cout<<Print_In_Center("___\b\b");
    string str;
    int command;
    cin.ignore(0,'\n');
    getline(cin,str);
    command=curpage.translate(str);
    while(command==0)//输入错误指令
    {
        cout<<Print_In_Center("输入错误,请重新输入");
        Sleep(2000);
        cout<<Print_In_Center("                  ");
        cout<<Print_In_Center("___\b\b");
        getline(cin,str);
        command=curpage.translate(str);
    }
    return command;
};
int System::Log_In_Page(const Page& curpage){
    system("cls");
    cout<<"请输入您的ID"<<endl;
    string _ID;
    getline(cin,_ID);
    while(Legal_ID(_ID)!=1||Unique_ID(_ID)!=0)
    {
        curmove(1);
	    cout<<"    ID错误,请重新输入";
        Sleep(2000);
        curmove(1);
        cout<<"                                                 ";
        curmove(1);
        getline(cin,_ID);
    }
    cout<<"请输入密码"<<endl;
    string _pwd;
    string tem;
    tem=Check_By_ID(_ID);
    getline(cin,_pwd);
    
    while(tem!=_pwd)
    {
        curmove(3);
	    cout<<"    密码不正确,请重新输入";
        Sleep(2000);
        curmove(3);
        cout<<"                                                 ";
        curmove(3);
        getline(cin,_pwd);
    }
    cout<<"登陆成功";
    User_ID=_ID;
    getch();
    return curpage.translate("home");
};
int System::Sign_Up_Page(const Page& curpage){
    system("cls");
    Customer mnew;
    cout<<"请输入您的姓名"<<endl;
    string Name;
    getline(cin,Name);
    mnew.name=Name;

    cout<<"请输入您的电话号码"<<endl;
    string Tele;
    getline(cin,Tele);
    bool legal=Legal_Tele(Tele),unique=Unique_Tele(Tele);
    string str1="格式错误",str2="已被使用";
    //注册时需保证合法且唯一
    while(!(legal&&unique))
    {
        curmove(3);
	    cout<<"该电话号码"<<((legal==0)?str1:str2)<<",请重新输入";
        Sleep(2000);
        curmove(3);
        cout<<"                                                               ";
        curmove(3);
        getline(cin,Tele);
        legal=Legal_Tele(Tele);
        unique=Unique_Tele(Tele);
    }
    mnew.tele=Tele;

    cout<<"请输入您的email"<<endl;
    string Email;
    getline(cin,Email);
    
    legal=Legal_Email(Email);
    unique=Unique_Email(Email);
    while(!(legal&&unique))
    {
        curmove(5);
	    cout<<"   该email"<<((legal==0)?str1:str2)<<",请重新输入";
        Sleep(2000);
        curmove(5);
        cout<<"                                                       ";
        curmove(5);
        getline(cin,Email);
        legal=Legal_Email(Email);
        unique=Unique_Email(Email);
    }
    mnew.email=Email;

    cout<<"请创建您的ID(只含有6-10位的数字或字母)"<<endl;
    string i_d;
    getline(cin,i_d);
    legal=Legal_ID(i_d);
    unique=Unique_ID(i_d);
    while(!(legal&&unique))
    {
        curmove(7);
	    cout<<"   该ID"<<((legal==0)?str1:str2)<<",请重新输入";
        Sleep(2000);
        curmove(7);
        cout<<"                                                       ";
        curmove(7);
        getline(cin,i_d);
        legal=Legal_ID(i_d);
        unique=Unique_ID(i_d);
    }
    mnew.id=i_d;

    cout<<"请创建您的密码(只含有6-10位的数字或字母)"<<endl;
    string Pwd;
    getline(cin,Pwd);
    legal=Legal_ID(i_d);
    while(!legal)
    {
        curmove(0,9);
	    cout<<"该密码不符合规定 ,请重新输入";
        Sleep(2000);
        curmove(0,9);
        cout<<"                                                       ";
        curmove(0,9);
        getline(cin,Pwd);
        legal=Legal_ID(i_d);    
    }
    mnew.pwd=Pwd;

    mnew.Write();
    cout<<"注册完成";
    getch();
    return curpage.translate("back");
};
int System::Forget_Page(const Page& curpage){
    return 0;
};
int System::Home_Page(const Page& curpage){
    system("cls");
    cout<<Print_In_Center(User_ID+"的主页");
    curmove(10);
    cout<<Print_In_Center("输入1查看航班信息\n");
    cout<<Print_In_Center("输入2查看个人信息\n");
    cout<<Print_In_Center("输入3查看订单信息\n");
    cout<<Print_In_Center("___\b\b");

    string str;
    getline(cin,str);
    int command=curpage.translate(str);
    while(command==0)//输入错误指令
    {
        cout<<Print_In_Center("输入错误,请重新输入");
        Sleep(2000);
        cout<<Print_In_Center("                  ");
        cout<<Print_In_Center("___\b\b");
        getline(cin,str);
        command=curpage.translate(str);
    }
    return command;
}
int System::Search_Page(const Page& curpage){
    system("cls");
    cout<<Print_In_Center("航班搜索面板\n");

    return 0;
}
int System::Infor_Page(const Page& curpage){
    system("cls");
    Customer u;
    u.id=User_ID;
    u.Complete();
    cout<<Print_In_Center(u.id+"的用户信息面板\n");
    int cnt=Get_Center_Position(":       ");
    vector<pair<string,string> >lis;
    lis.push_back(make_pair("用户名",u.id));
    lis.push_back(make_pair("姓名",u.name));
    lis.push_back(make_pair("电话",u.tele));
    lis.push_back(make_pair("邮箱",u.email));
    Print_List(lis,cnt);
    cout<<endl<<Print_In_Center("输入1或modify修改个人信息")<<endl;
    cout<<Print_In_Center("___\b\b");
    
    string str;
    getline(cin,str);
    int command=curpage.translate(str);
    while(command==0)//输入错误指令
    {
        cout<<Print_In_Center("输入错误,请重新输入");
        Sleep(2000);
        cout<<Print_In_Center("                  ");
        cout<<Print_In_Center("___\b\b");
        getline(cin,str);
        command=curpage.translate(str);
    }
    return command;
}
int System::Modify_Page(const Page& curpage){
    Customer u;
    u.id=User_ID;
    system("cls");
    int command=0;
    while(command!=curpage.translate("back")){
        u.Complete();
        cout<<Print_In_Center(u.id+"的信息修改面板\n");
        curmove(10);
        cout<<Print_In_Center("输入pwd修改密码\n");
        cout<<Print_In_Center("输入name修改姓名\n");
        cout<<Print_In_Center("输入tele修改电话号码\n");
        cout<<Print_In_Center("输入email修改邮箱\n");
        cout<<Print_In_Center("输入sex修改性别\n");
        cout<<Print_In_Center("输入back回到上一级\n");
        cout<<Print_In_Center("___\b\b");
        string str;
        getline(cin,str);
        int command=curpage.translate(str);
        while(command>=0)//输入错误指令
        {
            cout<<Print_In_Center("输入错误,请重新输入");
            Sleep(2000);
            cout<<Print_In_Center("                  ");
            cout<<Print_In_Center("___\b\b");
            getline(cin,str);
            command=curpage.translate(str);
        }
        if(command==curpage.translate("pwd")){
            
        }
        else if(command==curpage.translate("name")){

        }
        else if(command==curpage.translate("tele")){

        }
        else if(command==curpage.translate("email")){

        }
        else if(command==curpage.translate("sex")){

        }
    }
    return command;
}

int System::Order_Page(const Page& curpage){

    return 0;
}
int System::Buy_Page(const Page& curpage){
    return 0;
}