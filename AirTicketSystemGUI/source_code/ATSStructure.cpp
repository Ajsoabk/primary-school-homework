#include <iostream>
#include <algorithm>
#include <malloc.h>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include "ATS.h"
using std::string;
using std::transform;
namespace pg{
    Page welcome,mlog,login,signup,forget,home,search,infor,order,buy,modify;

};

/*

*/
int Page::translate(string str)const {
    transform(str.begin(),str.end(),str.begin(),::tolower);
    if(options.count(str)){
        return options.at(str);
    }
    else return 0;//
}
System::System(){
    pg::welcome.Ini_Welcome();
    pg::welcome.funcPtr=Welcome_Page;

    pg::mlog.Ini_Log();
    pg::mlog.funcPtr=Log_Page;

    pg::login.Ini_Log_In();
    pg::login.funcPtr=Log_In_Page;

    pg::signup.Ini_Sign_Up();
    pg::signup.funcPtr=Sign_Up_Page;

    pg::forget.Ini_Forget();
    pg::forget.funcPtr=Forget_Page;

    pg::home.Ini_Home();
    pg::home.funcPtr=Home_Page;

    pg::search.Ini_Search();
    pg::search.funcPtr=Search_Page;

    pg::infor.Ini_Infor();
    pg::infor.funcPtr=Infor_Page;

    pg::modify.Ini_Modify();
    pg::modify.funcPtr=Modify_Page;

    pg::order.Ini_Order();
    pg::order.funcPtr=Order_Page;
    
    pg::buy.Ini_Buy();
    pg::buy.funcPtr=Buy_Page;

    page=pg::welcome;
}
Page::Page(){
    options.insert(make_pair("back",-2));
    options.insert(make_pair("exit",-1));
}

void Page::Ini_Welcome(){
    options.insert(make_pair("log",1));

    optionsPtr.insert(make_pair(translate("log"),&(pg::mlog)));
}
void Page::Ini_Log(){
    //把用户在当前页面的字符串类型的输入映射到整数类型的命令
    options.insert(make_pair("login",1));
    options.insert(make_pair("1",1));
    options.insert(make_pair("signup",2));
    options.insert(make_pair("2",2));
    options.insert(make_pair("forget",3));
    options.insert(make_pair("3",3));
    

    //把整数类型的命令映射到指向对应页面的指针
    optionsPtr.insert(make_pair(translate("login"),&(pg::login)));
    optionsPtr.insert(make_pair(translate("signup"),&(pg::signup)));
    optionsPtr.insert(make_pair(translate("forget"),&(pg::forget)));
}
void Page::Ini_Log_In(){
    options.insert(make_pair("home",1));
    
    optionsPtr.insert(make_pair(translate("home"),&(pg::home)));
}
void Page::Ini_Sign_Up(){
    
}
void Page::Ini_Forget(){
    
}
void Page::Ini_Home(){
    options.insert(make_pair("search",1));
    options.insert(make_pair("1",1));
    options.insert(make_pair("infor",2));
    options.insert(make_pair("2",2));
    options.insert(make_pair("order",3));
    options.insert(make_pair("3",3));
    
    optionsPtr.insert(make_pair(translate("search"),&(pg::search)));
    optionsPtr.insert(make_pair(translate("infor"),&(pg::infor)));
    optionsPtr.insert(make_pair(translate("order"),&(pg::order)));
}
void Page::Ini_Search(){
    options.insert(make_pair("buy",2));

    optionsPtr.insert(make_pair(2,&(pg::buy)));
}
void Page::Ini_Infor(){
    options.insert(make_pair("modify",1));
    options.insert(make_pair("1",1));

    optionsPtr.insert(make_pair(translate("modify"),&(pg::modify)));
}
void Page::Ini_Order(){
    
}
void Page::Ini_Buy(){
    
}
void Page::Ini_Modify(){
    options.insert(make_pair("id",-2));
    options.insert(make_pair("pwd",-3));
    options.insert(make_pair("name",-4));
    options.insert(make_pair("sex",-5));
    options.insert(make_pair("tele",-6));
    options.insert(make_pair("email",-7));

}